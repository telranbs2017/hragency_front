export const environment = {
  production: true,
  backEndHttpUrl: '/api/',
  backEndHttpsUrl: '/api/',
  backEndHttpStatic: '/'
};
