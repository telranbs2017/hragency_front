import { Injectable, NgZone } from '@angular/core';
import { Applicant } from '../models/applicant';
import { ApplicantSkill } from '../models/applicant-skill';
import { ApplicantProject } from '../models/applicant-project';
import { SearchRequest } from '../models/search-request';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApplicantProjectDto } from '../models/dto/ApplicantProjectDto';
import 'rxjs/add/operator/map';
import { UserRole } from '../models/enums/user-role';
import { HideableField } from '../models/hideable-field';
import { Skill } from '../models/skill';
import { ProfessionalArea } from '../models/enums/professional-area';
import { ApplicantEducation } from '../models/applicant-education';
import { ApplicantLanguage } from '../models/applicant-language';
import { CompanyType } from '../models/enums/company-type';
import { Region } from '../models/enums/region';
import { ApplicantSkillDtoRequest } from '../models/dto/ApplicantSkillDtoRequest';

import { ApplicantDto } from '../models/dto/ApplicantDto';
import { SearchResponse } from './interfaces/i-search.service';

@Injectable()
export class ApplicantService {

  constructor(private http: HttpClient, private zone: NgZone) {
  }

  find(id: number): Promise<Applicant> {
    return this.http.get(environment.backEndHttpsUrl + 'applicants/' + id,
      {
        withCredentials: true,
      }).map((res) => {
      return this.mapToApplicant(res);
    })
      .toPromise();
  }

  search(request: SearchRequest): Promise<SearchResponse> {
    const myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    return this.http.get<SearchResponse>(
      environment.backEndHttpsUrl + 'applicants',
      {
        headers: myHeaders,
        withCredentials: true,
        params: {
          data: JSON.stringify({
            requestData: request,
            extractDeep: true,
            offset: request.offset,
            limit: request.limit
          })
        }
      }
    ).map(response => {
      const arr = [];
      response.list.forEach(applicant => arr.push(this.mapToApplicant(applicant)));
      return {list: arr, count: response.count};
    }).toPromise();
  }


  findBy(params: any): Applicant[] {
    return null;
  }

  create(applicant: Applicant): Promise<any> {
    return this.http.post(environment.backEndHttpsUrl + 'applicants',
      applicant,
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  update(applicant: Applicant): Promise<any> {
    return this.http.patch(environment.backEndHttpsUrl + 'applicants/',
      JSON.stringify(this.mapToApplicantDto(applicant)),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  remove(id: number) {

  }

  findSkill(applicantSkill: ApplicantSkill): ApplicantSkill {
    return null;
  }

  findSkills(applicantId: number): ApplicantSkill[] {
    return null;
  }

  createSkill(applicantSkillDTO: ApplicantSkillDtoRequest): Promise<any> {
    return this.http.post(environment.backEndHttpsUrl + 'applicantSkills',
      JSON.stringify(applicantSkillDTO),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  updateSkill(applicantSkillDTO: ApplicantSkillDtoRequest): Promise<any> {
    return this.http.patch(environment.backEndHttpsUrl + 'applicantSkills',
      JSON.stringify(applicantSkillDTO),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  removeSkill(skillId): Promise<any> {
    return this.http.delete(environment.backEndHttpsUrl + 'applicantSkills/' + skillId,
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  findProject(applicantProject: ApplicantProject): ApplicantProject {
    return null;
  }

  findProjects(applicantId: number): ApplicantProject[] {
    return null;
  }

  createProject(applicantProject: ApplicantProject): Promise<any> {
    return this.http.post(environment.backEndHttpsUrl + 'applicantProjects',
      JSON.stringify(new ApplicantProjectDto(0,
        applicantProject.title, applicantProject.description,
        applicantProject.company.value, applicantProject.dates, applicantProject.company.hidden)),
      {
        withCredentials: true,
        headers:
          new HttpHeaders({'Content-Type': 'application/json'})
      }
    ).toPromise();
  }

  updateProject(applicantProject: ApplicantProject) {
    return this.http.patch(environment.backEndHttpsUrl + 'applicantProjects',
      JSON.stringify(this.mapToApplicantProjectDto(applicantProject)),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'})
      }).toPromise();
  }

  removeProject(applicantProjectIid: number) {
    return this.http.delete(environment.backEndHttpsUrl + 'applicantProjects/' + applicantProjectIid,
      {
        withCredentials: true
      }).toPromise();
  }

  uploadPhoto(photo: FormData): any {
    return this.http.post(environment.backEndHttpsUrl + 'userImage',
      photo,
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Disposition': 'form-data'})
      }).toPromise();
  }

  uploadCv(cv: FormData): Promise<any> {
    return this.http.post(environment.backEndHttpsUrl + 'applicantCV',
      cv,
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Disposition': 'form-data'})
      }).toPromise();
  }

  downloadCv(cv: number): Promise<any> {
    const headers = new HttpHeaders();
    headers.append('trailer', 'Max-Forwards');
    return this.http.get(environment.backEndHttpsUrl + 'applicantCV/' + cv,
      {
        withCredentials: true,
        responseType: 'blob',
        headers: headers
      }).toPromise();
  }

  mapToApplicant(applicantDto: any): Applicant {
    const tmp = new Applicant(applicantDto.id, applicantDto.email.value);
    tmp.roleId = UserRole.Applicant;
    tmp.email = new HideableField(applicantDto.email.value, applicantDto.email.hidden === 1);
    tmp.imageUrl = applicantDto.imageUrl ? (environment.backEndHttpStatic + applicantDto.imageUrl) : 'assets/img/profile-pictures.png';
    /*tmp.imageUrl = applicantDto.imageUrl ? applicantDto.imageUrl : 'assets/img/profile-pictures.png';*/
    tmp.verificationStatus = applicantDto.verificationStatus;
    tmp.verificationDate = applicantDto.verificationDate;
    tmp.contactEmail = applicantDto.contactEmail;
    tmp.firstName = new HideableField(applicantDto.firstName.value, applicantDto.firstName.hidden === 1);
    tmp.lastName = new HideableField(applicantDto.lastName.value, applicantDto.lastName.hidden === 1);
    tmp.title = applicantDto.title;
    tmp.phone = new HideableField(applicantDto.phone.value, applicantDto.phone.hidden === 1);
    tmp.birthday = new HideableField(applicantDto.birthday.value * 1000, applicantDto.birthday.hidden === 1);
    tmp.gender = new HideableField(applicantDto.gender.value === 1, applicantDto.gender.hidden === 1);
    tmp.city = new HideableField(applicantDto.city.value, applicantDto.city.hidden === 1);
    tmp.citizenship = new HideableField(applicantDto.citizenship.value, applicantDto.citizenship.hidden === 1);
    const skills = [];
    applicantDto.skills.forEach(skill => {
      skills.push(this.mapToApplicantSkill(skill));
    });
    tmp.skills = skills;
    const projects = [];
    applicantDto.projects.forEach(project => {
      projects.push(this.mapToProject(project));
    });
    tmp.projects = projects;

    const professionalAreas = [];
    applicantDto.professionalAreas.forEach(area => {
      professionalAreas.push(
        this.mapToProfessionalArea(area));
    });
    tmp.professionalAreas = professionalAreas;

    const educations = [];
    applicantDto.educations.forEach(education => {
      educations.push(this.mapToEducation(education));
    });
    tmp.educations = educations;
    const languages = [];
    applicantDto.languages.forEach(language => {
      languages.push(this.mapToLanguage(language));
    });
    tmp.languages = languages;

    const companyTypes = [];
    applicantDto.acceptableCompanyTypes.forEach(company => {
      companyTypes.push(this.mapToCompanyType(company));
    });
    tmp.acceptableCompanyTypes = companyTypes;

    const regions = [];
    applicantDto.regionsOfInterest.forEach(region => {
      regions.push(this.mapToRegionOfIntrest(region));
    });
    tmp.regionsOfInterest = regions;

    tmp.minSalaryExpectation = applicantDto.minSalaryExpectation;
    tmp.maxSalaryExpectation = applicantDto.maxSalaryExpectation;
    tmp.availableAfterDays = applicantDto.availableAfterDays;
    tmp.cvUrl = applicantDto.cvUrl;
    tmp.otherInfo = applicantDto.otherInfo;
    tmp.dateExpires = applicantDto.dateExpires;
    tmp.counterViewTotal = applicantDto.counterViewTotal;
    tmp.counterViewSearches = applicantDto.counterSearches;
    return tmp;
  }

  private mapToApplicantSkill(applicantSkill: any): ApplicantSkill {
    const skill = new Skill(applicantSkill.skill.id, applicantSkill.skill.name);
    const tmp = new ApplicantSkill(skill, applicantSkill.experience);
    tmp.modifiedAt = applicantSkill.modifiedAt;
    return tmp;
  }

  private mapToProject(project: any): ApplicantProject {
    return new ApplicantProject(project.id, project.title, project.description,
      new HideableField(project.company.value, project.company.hidden === 1), project.dates);
  }

  private mapToProfessionalArea(professionalArea: any): ProfessionalArea {
    return professionalArea.id;
  }

  private mapToEducation(education: any): ApplicantEducation {
    return new ApplicantEducation(education.id, education.description, education.university, education.dates);
  }

  private mapToLanguage(language: any): ApplicantLanguage {
    return new ApplicantLanguage(language.id, language.language, language.level);
  }

  private mapToCompanyType(companyType: any): CompanyType {
    return companyType.id;
  }

  private mapToRegionOfIntrest(region: any): Region {
    return region.id;
  }

  mapToApplicantDto(applicant: Applicant): ApplicantDto {
    const skills = [];
    applicant.skills.forEach(skill => skills.push({id: skill.skill.id, experience: skill.experience}));
    const projects = [];
    applicant.projects.forEach(project => {
      projects.push(this.mapToApplicantProjectDto(project));
    });
    const professionalAreas = [];
    applicant.professionalAreas.forEach(area => {
      professionalAreas.push(this.mapToProfesionalAreaDto(area));
    });
    const educations = [];
    applicant.educations.forEach(education => {
      educations.push(this.mapToEducationDto(education));
    });
    const languages = [];
    applicant.languages.forEach(language => {
      languages.push(this.mapToLanguageDto(language));
    });
    const companyTypes = [];
    applicant.acceptableCompanyTypes.forEach(companyType => {
      companyTypes.push(companyType);
    });
    const regions = [];
    applicant.regionsOfInterest.forEach(region => {
      regions.push(this.mapToRegionDto(region));
    });
    return new ApplicantDto(applicant.id, applicant.roleId,
      {value: applicant.email.value, hidden: applicant.email.hidden ? 1 : 0},
      applicant.imageUrl.replace(environment.backEndHttpStatic, ''), applicant.verificationStatus,
      applicant.verificationDate, applicant.contactEmail,
      {value: applicant.firstName.value, hidden: applicant.firstName.hidden ? 1 : 0},
      {value: applicant.lastName.value, hidden: applicant.lastName.hidden ? 1 : 0},
      applicant.title, {value: applicant.phone.value, hidden: applicant.phone.hidden ? 1 : 0},
      {value: applicant.birthday.value / 1000, hidden: applicant.birthday.hidden ? 1 : 0},
      {value: applicant.gender.value ? 1 : 0, hidden: applicant.gender.hidden ? 1 : 0},
      {value: applicant.city.value, hidden: applicant.city.hidden ? 1 : 0},
      {value: applicant.citizenship.value, hidden: applicant.citizenship.hidden ? 1 : 0},
      skills, projects, professionalAreas, educations, languages, companyTypes,
      regions, applicant.minSalaryExpectation, applicant.maxSalaryExpectation,
      applicant.availableAfterDays, applicant.cvUrl, applicant.otherInfo,
      applicant.dateExpires, applicant.counterViewTotal, applicant.counterViewSearches
    )
      ;
  }

  private mapToApplicantProjectDto(applicantProject: ApplicantProject): ApplicantProjectDto {
    return new ApplicantProjectDto(applicantProject.id,
      applicantProject.title, applicantProject.description,
      applicantProject.company.value || '', applicantProject.dates, applicantProject.company.hidden ? 1 : 0);
  }

  private mapToProfesionalAreaDto(area: ProfessionalArea): any {
    return {id: area, name: ProfessionalArea[area]};
  }

  private mapToEducationDto(education: ApplicantEducation): any {
    return {
      id: education.id,
      description: education.description,
      dates: education.dates,
      university: education.university
    };
  }

  private mapToLanguageDto(language: ApplicantLanguage): any {
    return {id: language.id, level: language.level, language: language.language};
  }

  private mapToCompanyTypeDto(company: CompanyType): any {
    return {id: company, name: CompanyType[company]};
  }

  private mapToRegionDto(region: Region): any {
    return {id: region, name: Region[region]};
  }
}
