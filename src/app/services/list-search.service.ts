import { Injectable } from '@angular/core';
import { IListManager } from './interfaces/i-list-manager';
import { ISearchService } from './interfaces/i-search.service';
import { DefaultSettings } from '../models/enums/default-settings';
import {
  FieldInComponent,
  SearchRequestFormSettings
} from '../components/search-request-form/search-request-form-settings';
import { ToastService } from './toast.service';

@Injectable()
export class ListSearchService {
  // Status flag for loading animation
  isLoading: boolean;

  // Manager Abstract List Component
  listManager: IListManager;

  // Manager Search Request Form Component
  searchService: ISearchService;

  // Objects, which will be shown in a list (applicants, requests...)
  objects: Array<any>;

  // Total number of entries in DB, suitable for request params
  count: number;

  // Pagination variables
  // Pages - List of page's captions exactly to fill all of objects
  pages: string[];
  // currentPage - current (active) page
  currentPage: number;
  // fitToPage - number of elements that fit to page
  fitToPage: number;

  // Request form settings
  settings: SearchRequestFormSettings;

  // Form Fields
  fields: FieldInComponent[];

  // array for storage data between different lists
  pocket: ListSearchService[];

  constructor(private toastService: ToastService) {
    this.isLoading = true;
    this.objects = [];
    this.count = 0;
    this.pages = [];
    this.currentPage = 0;
    this.fitToPage = DefaultSettings.objectsPerPage;

    this.settings = new SearchRequestFormSettings();
    this.fields = [];

    this.pocket = [];
  }

  onInitList(id: number, listManager: IListManager): void {
    if (this.pocket[id] !== undefined) {
      this.listManager = this.pocket[id].listManager || listManager;
      this.pages = this.pocket[id].pages || [];
      this.currentPage = this.pocket[id].currentPage || 0;
      this.fitToPage = this.pocket[id].fitToPage || DefaultSettings.objectsPerPage;
      this.objects = this.pocket[id].objects || [];
    } else {
      this.pocket[id] = new ListSearchService(this.toastService);
      this.listManager = listManager;
    }
  }

  onInitSearch(id: number, searchService: ISearchService, settings: SearchRequestFormSettings): void {
    if (this.pocket[id] !== undefined) {
      this.searchService = this.pocket[id].searchService || searchService;
      this.settings = this.pocket[id].settings || settings;
      this.fields = this.pocket[id].fields || [];
    } else {
      this.pocket[id] = new ListSearchService(this.toastService);
      this.searchService = searchService;
      this.settings = settings;
      this.load();
    }
  }

  onDestroyList(id: number) {
    this.pocket[id].listManager = this.listManager;
    this.pocket[id].pages = this.pages;
    this.pocket[id].currentPage = this.currentPage;
    this.pocket[id].fitToPage = this.fitToPage;
    this.pocket[id].objects = this.objects;

    this.listManager = undefined;
    this.pages = [];
    this.objects = [];
  }

  onDestroySearch(id: number) {
    this.pocket[id].searchService = this.searchService;
    this.pocket[id].settings = this.settings;
    this.pocket[id].fields = this.fields;

    this.searchService = undefined;
    this.settings = undefined;
    this.fields = [];
  }

  reloadPages(qty: number): void {
    let i = 0;
    this.pages = [];
    while (qty > 0) {
      this.pages.push('' + ++i);
      qty -= this.fitToPage;
    }
  }

  load(): void {
    this.loadObjects(0, this.fitToPage);
  }

  loadObjects(offset: number, limit: number): void {
    this.pages = [];
    this.isLoading = true;
    this.searchService.request.offset = offset;
    this.searchService.request.limit = limit;
    this.searchService.send()
      .then(response => {
        this.isLoading = false;
        this.objects = response.list;
        this.count = response.count;
        this.reloadPages(response.count);
      })
      .catch(error => {
        console.log(error);
        this.toastService.toast('Data didn\'t load. Please try later');
        this.isLoading = false;
      });
  }

}
