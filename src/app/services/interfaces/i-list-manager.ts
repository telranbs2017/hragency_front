export interface IListManager {
  // Names of properties, which will be  columns in table view
  tableColumns: IColumns[];
  // Actions. For every action will be created a button in table row or in a card.
  // If this button will be pressed, the function apply will be called (see below)
  // For actions use icon names from: http://materializecss.com/icons.html
  actions: string[];
  /*
   *  getters - functions for value processing.
   *  Example:
   *    getters = { date: this.toDate; }
   *    toDate(val) { return new Date(val); }
   */
  getters: {};
  /*
   * hasUserApprove - if true - show UserApproveComponent
   */
  hasUserApprove: boolean;

  // Makes link for action button.
  // action - 'action name', one of  actions
  // obj - object, which button is pressed, one of objects
  // Example:  return '/testrequest/' + obj['id'];
  getActionLink(action: string, obj: any): string;

  // Makes link for click on objects row.
  getClickLink(obj: any): string;

}

export interface IColumns {
  title: string;
  key: string;
}
