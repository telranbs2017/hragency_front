import { SearchRequest } from '../../models/search-request';
import { SearchRequestFormSettings } from '../../components/search-request-form/search-request-form-settings';

export interface ISearchService {
  request: SearchRequest;

  /*
  send request using entity service, return Promise with array of objects
  Calling from list manager Pagination
   */
  send(request?: SearchRequest): Promise<SearchResponse>;

}

export interface SearchResponse {
  list: any[];
  count: number;
}
