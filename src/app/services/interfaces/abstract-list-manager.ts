import { IListManager, IColumns } from './i-list-manager';

export abstract class AbstractListManager implements IListManager {
  tableColumns: IColumns[];
  actions: string[];
  getters: {};
  hasUserApprove: boolean;

  constructor() {
    this.tableColumns = [];
    this.actions = [];
    this.hasUserApprove = false;
  }

  getActionLink(action: string, obj: any): string {
    return '';
  }

  getClickLink(obj: any): string {
    return '';
  }
}
