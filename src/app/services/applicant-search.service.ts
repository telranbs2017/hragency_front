import { ApplicantService } from './applicant.service';
import { ISearchService, SearchResponse } from './interfaces/i-search.service';
import { SearchRequest } from '../models/search-request';

export class ApplicantSearchService implements ISearchService {
  request: SearchRequest;

  constructor(private applicantService: ApplicantService) {
    this.request = new SearchRequest();
  }

  send(request?: SearchRequest): Promise<SearchResponse> {
    if (request !== undefined) {
      this.request = request;
    }
    if (this.request === undefined) {
      this.request = new SearchRequest();
    }
    return this.applicantService.search(this.request);
  }

}
