import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Skill } from '../models/skill';

@Injectable()
export class SkillService {
  dictionary: Skill[];

  constructor(private http: HttpClient) {
    this.dictionary = [];
    this.http.get<Skill[]>(environment.backEndHttpsUrl + 'skills',
      {
        withCredentials: true
      }).toPromise()
      .then(skills => this.dictionary = skills)
      .then(() => this.dictionary.sort());
  }


  find(id: number): Promise<Skill> {
    const arr = this.dictionary.filter((skill) => skill.id === id);
    if (arr.length > 0) {
      return Promise.resolve(arr[0]);
    }
    this.http.get<Skill>(environment.backEndHttpsUrl + 'skills/' + id,
      {
        withCredentials: true,
      });
  }

  search({id, name}): Skill[] {
    return null;
  }

  create(skill: Skill): Promise<number> {
    return this.http.post<number>(environment.backEndHttpsUrl + 'skills',
      {name: skill.name},
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      })
      .toPromise();
  }

  update(skill: Skill) {

  }

  remove(id: number) {

  }
}
