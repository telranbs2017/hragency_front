import { Applicant } from '../models/applicant';
import { Injectable } from '@angular/core';
import { Skill } from '../models/skill';
import { SkillService } from './skill.service';
import { CompanyType } from '../models/enums/company-type';

@Injectable()
export class LocalApplicantService {

  private _currentApplicant: Applicant = new Applicant(0, '');
  private _isEditable = false;
  private _skillArray: Skill[];
  private companyDictionary: CompanyType[];
  public savingError: String;
  public savingState = false;

  constructor(public skillService: SkillService) {
    this._skillArray = this.skillService.dictionary;
  }

  get currentApplicant(): Applicant {
    return this._currentApplicant;
  }

  set currentApplicant(value: Applicant) {
    this._currentApplicant = value;
  }

  get isEditable(): boolean {
    return this._isEditable;
  }

  set isEditable(value: boolean) {
    this._isEditable = value;
  }

  get skillArray(): Skill[] {
    return this._skillArray;
  }

  set skillArray(value: Skill[]) {
    this._skillArray = value;
  }

  skillArrayCleanUp(skillArray, skill) {
    for (let i = 0; i < this._skillArray.length; i++) {
      if (this._skillArray[i].id === skill.id) {
        this._skillArray.splice(i, 1);
        break;
      }
    }
  }

}
