import { Injectable } from '@angular/core';
import { Applicant } from '../models/applicant';
import { Employer } from '../models/employer';
import { CompanyType } from '../models/enums/company-type';
import { ProfessionalArea } from '../models/enums/professional-area';
import { Skill } from '../models/skill';
import { HideableField } from '../models/hideable-field';
import { Random } from '../utils/random';
import { ApplicantSkill } from '../models/applicant-skill';
import { ApplicantProject } from '../models/applicant-project';
import { ApplicantLanguage } from '../models/applicant-language';
import { ApplicantEducation } from '../models/applicant-education';
import { DTOHardcodeService } from './dto-hardcode.service';
import { Region } from '../models/region';

@Injectable()
export class HardcodeService {

  constructor() {
  }

  getApplicants(): Applicant[] {

    const res = [];
    const applicantsDTO = DTOHardcodeService.getApplicantsDTO();
    const cities = this.getCities();
//    const professionalAreas = this.getProfessionalAreas();
    const skills = this.getSkills();
//    const companyTypes = this.getCompanyTypes();
    let projectCounter = 0;
    let applicantLanguageCounter = 0;
    let applicantEducationCounter = 0;
    applicantsDTO.forEach(applicantDTO => {
      const applicant = new Applicant(applicantDTO.id, applicantDTO.email);
      applicant.firstName = new HideableField(applicantDTO.firstName, Random.nextDouble(0, 1) < 0.7);
      applicant.lastName = new HideableField(applicantDTO.lastName, Random.nextDouble(0, 1) < 0.7);
      applicant.gender = new HideableField(applicantDTO.gender === 'Male', Random.nextDouble(0, 1) < 0.7);
      applicant.phone = new HideableField(applicantDTO.phone, Random.nextDouble(0, 1) < 0.7);
      applicant.city = new HideableField(cities[Random.nextInt(0, cities.length)], Random.nextDouble(0, 1) < 0.7);
      applicant.citizenship = new HideableField('Israel', true);
      applicant.minSalaryExpectation = Random.nextInt(10, 20) * 1000;
      applicant.maxSalaryExpectation = Random.nextInt(applicant.minSalaryExpectation / 1000, 30) * 1000;
      applicant.birthday = new HideableField(
        Random.nextDate(new Date(1960, 0, 1), new Date(2000, 0, 1)).getTime(),
        Random.nextDouble(0, 1) < 0.7); // applicantDTO.birthday;
//      applicant.professionalAreas.push(professionalAreas[Random.nextInt(0, professionalAreas.length)]);
      const numberOfSkills = Random.nextInt(1, 20);
      for (let i = 0; i < numberOfSkills; i++) {
        applicant.skills.push(new ApplicantSkill(skills[Random.nextInt(0, skills.length)], Random.nextInt(0, 10)));
      }
      let currentDate = new Date();
      applicantDTO.projects.forEach(projectDTO => {
        const nextDate = Random.nextDate(new Date(currentDate.getFullYear() - 3, 0, 1), currentDate);
        const period = nextDate.toDateString().substr(4, 3) + ' ' + nextDate.toDateString().substr(11, 4) +
          ' - ' + currentDate.toDateString().substr(4, 3) + ' ' + currentDate.toDateString().substr(11, 4);
        currentDate = nextDate;
        const nextProject = new ApplicantProject(
          projectCounter++,
          projectDTO.title,
          projectDTO.title + ' ' + projectDTO.description,
          projectDTO.company,
          period);
        applicant.projects.push(nextProject);
      });
      // applicant.title = applicant.professionalAreas[0].name + ' with ' +
      //   ((new Date()).getFullYear() - currentDate.getFullYear()) + ' year\'s experience';
      applicantDTO.languages.forEach(languageDTO => {
        const applicantLanguage = new ApplicantLanguage(applicantLanguageCounter++, languageDTO, 0);
        applicant.languages.push(applicantLanguage);
      });
      currentDate = new Date(currentDate.getFullYear() - 10, 0, 1);
      applicantDTO.educations.forEach(educationDTO => {
        const startDate = Random.nextDate(currentDate, new Date()).getFullYear();
        const endDate = startDate + Random.nextInt(0, 6);
        const applicantEducation = new ApplicantEducation(applicantEducationCounter++,
          educationDTO.description, educationDTO.univercity, '' + startDate + ' - ' + endDate);
        applicant.educations.push(applicantEducation);
      });
      // companyTypes.forEach(companyType => applicant.acceptableCompanyTypes.push(companyType));
      res.push(applicant);
    });
    return res;
  }

  getEmployers(): Employer[] {
    // const companyTypes = this.getCompanyTypes();
    const employersDTO = DTOHardcodeService.getEmployersDTO();
    let employerCount = 0;
    const res = [];
    employersDTO.forEach(employerDTO => {

      const employer = new Employer(employerCount++, employerDTO._email);
      employer.name = employerDTO._name;
      employer.phone = employerDTO._phone;
      // employer.companyType = companyTypes[Random.nextInt(0, companyTypes.length)];
      employer.specializations = employerDTO._specializations;
      employer.region = Random.nextInt(0, Object.keys(Region).length);
      employer.founded = employerDTO._founded;
      employer.staffNumber = employerDTO._staffNumber * 100;
      employer.address = employerDTO._address;
      employer.webSite = employerDTO._webSite;
      res.push(employer);
    });
    return res;
  }

  getCities(): string[] {
    return [
      'Jerusalem',
      'Tel-Aviv',
      'Haifa',
      'Rishon LeZion',
      'Petakh Tikva',
      'Ashdod',
      'Netania',
      'Beer-Sheva',
      'Holon',
      'Bnei Brak',
      'Ramat Gan',
      'Rehovot',
      'Ashkelon',
      'Bat Yam',
      'Beit Shemesh',
      'Kfar Saba',
      'Herzliya',
      'Hadera',
      'Modi\'in-Maccabim-Re\'ut',
      'Nazareth',
      'Ramla',
      'Lod',
      'Ra\'anana',
      'Modi\'in Illit',
      'Rahat',
      'Hod HaSharon',
      'Givatayim',
      'Kiryat Ata',
      'Nahariya',
      'Umm al-Fahm',
      'Kiryat Gat',
      'Beitar Illit',
      'Eilat',
      'Ness Ziona',
      'Acre',
      'Afula',
      'El\'ad',
      'Rosh HaAyin',
      'Karmiel',
      'Ramat HaSharon',
      'Yavne',
      'Tiberias',
      'Tayibe',
      'Kiryat Motzkin',
      'Shefa-\'Amr',
      'Nazareth Illit'
    ];
  }

  // getCompanyTypes(): CompanyType[] {
  //   return [
  //     new CompanyType(1, 'Regular IT Company'),
  //     new CompanyType(2, 'Outsource company'),
  //     new CompanyType(3, 'Startup company'),
  //     new CompanyType(4, 'HeadHunter Company'),
  //     new CompanyType(5, 'HR Company')
  //   ];
  // }

  getLanguagesLevels(): string[] {
    return [
      'nativ',
      'advance',
      'with dictionary',
    ];
  }


  // getProfessionalAreas(): ProfessionalArea[] {
  //   return [
  //     new ProfessionalArea(1, 'Hardware developer'),
  //     new ProfessionalArea(2, 'C developer'),
  //     new ProfessionalArea(3, 'C++ developer'),
  //     new ProfessionalArea(4, 'C# developer'),
  //     new ProfessionalArea(5, '.NET developer'),
  //     new ProfessionalArea(6, 'Java developer'),
  //     new ProfessionalArea(7, 'Java fullstack developer'),
  //     new ProfessionalArea(8, 'Java backend developer'),
  //     new ProfessionalArea(9, 'PHP developer'),
  //     new ProfessionalArea(10, 'PHP fullstack developer'),
  //     new ProfessionalArea(11, 'Frontend JavaScript developer'),
  //     new ProfessionalArea(12, 'NodeJS developer'),
  //     new ProfessionalArea(13, 'Web developer'),
  //     new ProfessionalArea(14, 'System administrator'),
  //     new ProfessionalArea(15, 'Database administrator'),
  //     new ProfessionalArea(16, 'Devops engineer'),
  //     new ProfessionalArea(17, 'QA engineer'),
  //     new ProfessionalArea(18, 'Graphic designer'),
  //     new ProfessionalArea(19, 'Web designer'),
  //     new ProfessionalArea(20, 'Mobile android developer'),
  //     new ProfessionalArea(21, 'Mobile iOS developer'),
  //     new ProfessionalArea(22, 'Network architect'),
  //     new ProfessionalArea(23, 'Project manager')];
  // }

  /*
    getRegions(): Region[] {
      return [
        new Region(1, 'Jerusalem District'),
        new Region(2, 'Northern District'),
        new Region(3, 'Haifa District'),
        new Region(4, 'Central District'),
        new Region(5, 'Tel Aviv District'),
        new Region(6, 'Southern District'),
        new Region(7, 'Judea and Samaria Area')
      ];
    }
  */

  getSkills(): Skill[] {
    return [
      new Skill(1, 'UML'),
      new Skill(2, 'OOP/OOD'),
      new Skill(3, 'Eclipse'),
      new Skill(4, 'Visual Studio Code'),
      new Skill(5, 'WebStorm'),
      new Skill(6, 'Git'),
      new Skill(7, 'Subversion'),
      new Skill(8, 'HTML'),
      new Skill(9, 'CSS'),
      new Skill(10, 'JavaScript'),
      new Skill(11, 'jQuery'),
      new Skill(12, 'AJAX'),
      new Skill(13, 'TypeScript'),
      new Skill(14, 'AngularJS'),
      new Skill(15, 'Angular 2+'),
      new Skill(16, 'PHP'),
      new Skill(17, 'XML'),
      new Skill(18, 'Json'),
      new Skill(19, 'YII2'),
      new Skill(20, 'Bootstrap'),
      new Skill(21, 'C++'),
      new Skill(22, 'C#'),
      new Skill(23, '.NET'),
      new Skill(24, 'Python'),
      new Skill(25, 'Machine Learning'),
      new Skill(26, 'Java'),
      new Skill(27, 'JUnit'),
      new Skill(28, 'JSP'),
      new Skill(29, 'Servlets'),
      new Skill(30, 'Spring Framework'),
      new Skill(31, 'Hibernate'),
      new Skill(32, 'SQL'),
      new Skill(33, 'PostgreSQL'),
      new Skill(34, 'MySQL'),
      new Skill(35, 'Oracle SQL'),
      new Skill(36, 'SQL Server'),
      new Skill(37, 'SQLite'),
      new Skill(38, 'MongoDB'),
      new Skill(39, 'Redis'),
      new Skill(40, 'Cassandra'),
      new Skill(41, 'Elastic Search'),
      new Skill(42, 'Apache Kafka'),
      new Skill(43, 'MQ Rabbit'),
      new Skill(44, 'Windows'),
      new Skill(45, 'Linux'),
      new Skill(46, 'iOS'),
      new Skill(47, 'Android'),
      new Skill(48, 'VMware'),
      new Skill(49, 'VirtualBox'),
      new Skill(50, 'Routing'),
      new Skill(51, 'DHCP'),
      new Skill(52, 'DNS'),
      new Skill(53, 'Virtualization'),
      new Skill(54, 'Photoshop'),
      new Skill(55, 'Adobe Flash'),
      new Skill(56, 'Adobe Illustrator'),
      new Skill(57, 'Adobe After Effects')
    ];
  }

  getUserTypes(): any {
    return [
      {id: 1, name: 'Admin'},
      {id: 2, name: 'Applicant'},
      {id: 3, name: 'Employer'}
    ];
  }
}
