import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot,  RouterStateSnapshot } from '@angular/router';
import { UserService } from './user.service';
import { isUndefined } from 'util';
import { UserRole } from '../models/enums/user-role';
import { CurrentUser } from '../models/current-user';

@Injectable()
export class AuthGuardService implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (isUndefined(this.userService.user)) {
      if (localStorage.getItem('pavremember') === 'true') {
        // const data = this.login();
        //   if (data['id']) {
        //     this.userService.user = new CurrentUser(data['id'], data['roleId'], localStorage.getItem('email'), data['imageUrl']);
        //     localStorage.setItem('user', JSON.stringify(this.userService.user));
        //   }
        this.login()
        // this.userService.authenticate({'email' : localStorage.getItem('email') ,
        //   'password' : localStorage.getItem('password')})
        .then((data: any) => {
          if (data['id']) {
            this.userService.user = new CurrentUser(data['id'], data['roleId'], localStorage.getItem('email'), data['imageUrl']);
            localStorage.setItem('user', JSON.stringify(this.userService.user));
          }
        }, err => {
          console.log(err);
        });
      }else {
        this.userService.redirectUrl = state.url;
        this.router.navigate(['/login']);
        return false;
      }
    } else {
      return this.checkGuard(state);
    }
  }
  constructor(private router: Router, private userService: UserService) {
  }
  async login(): Promise<any> {
    const data = await this.userService.authenticate({'email' : localStorage.getItem('email') ,
            'password' : localStorage.getItem('password')});
    return data;
    }
  checkGuard(state: RouterStateSnapshot): boolean {
    if (state.url.indexOf('admin/') > 0 && this.userService.user.roleId === UserRole.Admin) { return true; }
    if (state.url.indexOf('applicant/') > 0 && this.userService.user.roleId === UserRole.Applicant) { return true; }
    if (state.url.indexOf('employer/') > 0 && this.userService.user.roleId === UserRole.Employer) { return true; }
    this.router.navigate(['']);
    return false;
  }
}
