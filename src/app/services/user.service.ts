import { Injectable } from '@angular/core';
import { CurrentUser } from '../models/current-user';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  private _user: CurrentUser;
  private _pingInterval;
  private _redirectUrl: string;

  constructor(private http: HttpClient) {
  }
  get redirectUrl(): string {
    return this._redirectUrl;
  }

  set redirectUrl(value: string) {
    this._redirectUrl = value;
  }

  get user(): CurrentUser {
    return this._user;
  }

  set user(value: CurrentUser) {
    this._user = value;
  }

  public startSession() {
    this._pingInterval = setInterval(this.ping, 1800000);
  }

  private ping() {
    this.http.get(
      environment.backEndHttpsUrl + 'ping',
      {withCredentials: true}
    );
  }

  register(userData): Promise<any> {
    return this.http.post(
      environment.backEndHttpsUrl + 'register',
      userData,
      {
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
        withCredentials: true
      }
    ).toPromise();
  }

  logout() {
    this.http.get(
      environment.backEndHttpsUrl + 'logout',
      {withCredentials: true}
    )
      .toPromise()
      .then(
        response => {
          this.user = null;
          if (this._pingInterval !== undefined) {
            this._pingInterval.clearInterval();
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  authenticate(userData): Promise<any> {
    return this.http.post(
      environment.backEndHttpsUrl + 'authenticate',
      userData,
      {
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
        withCredentials: true
      }
    ).toPromise();
  }

  approve(params: IVerification): Promise<any> {
    return this.http.patch(environment.backEndHttpsUrl + 'users/approve/',
      JSON.stringify(params),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

}

interface IVerification {
  id: number;
  verificationStatus: number;
}
