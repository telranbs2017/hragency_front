import { Employer } from '../models/employer';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchRequest } from '../models/search-request';
import { SearchResponse } from './interfaces/i-search.service';
import { UserRole } from '../models/enums/user-role';
import { HideableField } from '../models/hideable-field';
import { EmployerDto } from '../models/dto/employer-dto';

@Injectable()
export class EmployerService {

  constructor(private http: HttpClient) {
  }

  find(id: number): Promise<Employer> {
    return this.http.get(environment.backEndHttpsUrl + 'employers/' + id,
      {
        withCredentials: true
      }).map((employer) => {
      return this.mapToEmployer(employer);
    })
      .toPromise();
  }

  findBy({id, name, companyType}): Employer[] {
    return null;
  }

  search(request: SearchRequest): Promise<SearchResponse> {
    const myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    return this.http.get<SearchResponse>(
      environment.backEndHttpsUrl + 'employers',
      {
        headers: myHeaders,
        withCredentials: true,
        params: {
          data: JSON.stringify({
            requestData: request,
            extractDeep: true,
            offset: request.offset,
            limit: request.limit
          })
        }
      }
    ).map(response => {
      const arr = [];
      response.list.forEach(employer => arr.push(this.mapToEmployer(employer)));
      return {list: arr, count: response.count};
    }).toPromise();
  }

  create(employer: Employer): number {
    return null;
  }

  update(employer: Employer): Promise<any> {
    return this.http.patch(environment.backEndHttpsUrl + 'employers/',
      JSON.stringify(new EmployerDto(employer)),
      {
        withCredentials: true,
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
      }).toPromise();
  }

  remove(id: number) {

  }

  mapToEmployer(employerDTO: any) {
    const res = new Employer(0, '');
    res.id = employerDTO.id;
    res.roleId = UserRole.Employer;
    res.email = new HideableField<string>(employerDTO.email.value, employerDTO.email.hidden === 1);
    res.imageUrl = employerDTO.imageUrl ? employerDTO.imageUrl : 'assets/img/profile-pictures.png';
    res.verificationStatus = employerDTO.verificationStatus;
    res.verificationDate = employerDTO.verificationDate;
    res.contactEmail = employerDTO.contactEmail;
    res.name = employerDTO.name;
    res.phone = employerDTO.phone;
    res.companyType = +employerDTO.companyType;
    res.region = +employerDTO.region;
    res.specializations = employerDTO.specializations;
    res.founded = employerDTO.founded;
    res.staffNumber = employerDTO.staffNumber;
    res.address = employerDTO.address;
    res.webSite = employerDTO.webSite;

    return res;
  }

}

