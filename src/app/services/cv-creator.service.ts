import { Injectable } from '@angular/core';
import { Applicant } from '../models/applicant';
import * as Docxtemplater from 'docxtemplater';
import * as JsZip from 'jszip';
import * as jsZipUtils from 'jszip-utils';
import * as FileSaver from 'file-saver';

@Injectable()
export class CvCreatorService {

  constructor() {
  }

  generate(_applicant: Applicant, _template: string, expressions?: any) {
    const URI = '../../../assets/' + (_template.length ? _template : 'input.docx');
    jsZipUtils.getBinaryContent(URI, (error, content) => {
      if (error) {
        console.log(error);
      } else {
        const zip = new JsZip(content);
        const doc = new Docxtemplater().loadZip(zip);

        const applicantDTO = _applicant.getDTO(expressions || {});
        doc.setData(applicantDTO);
        try {
          doc.render();
        } catch (error) {
          console.log(error);
        }
        const buf = doc.getZip().generate({
          type: 'blob',
          mimeType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        FileSaver.saveAs(buf, _applicant.title + '-' + _applicant.firstName.value + '-' + _applicant.lastName.value + '.docx');
      }
    });
  }

  /*
    toDate(value: number): string {
      return (new Date(value)).toISOString().substring(0, 10);
    }
  */
}
