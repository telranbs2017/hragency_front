import { SearchRequest } from '../models/search-request';
import { ISearchService, SearchResponse } from './interfaces/i-search.service';
import { EmployerService } from './employer.service';

export class EmployerSearchService implements ISearchService {
  request: SearchRequest;

  constructor(private employerService: EmployerService) {
    this.request = new SearchRequest();
  }

  send(request?: SearchRequest): Promise<SearchResponse> {
    if (request !== undefined) {
      this.request = request;
    }
    if (this.request === undefined) {
      this.request = new SearchRequest();
    }
    return this.employerService.search(this.request);
  }

}
