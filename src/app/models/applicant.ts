import { User } from './user';
import { HideableField } from './hideable-field';
import { ApplicantSkill } from './applicant-skill';
import { ApplicantProject } from './applicant-project';
import { ProfessionalArea } from './enums/professional-area';
import { CompanyType } from './enums/company-type';
import { Region } from './enums/region';
import { ApplicantEducation } from './applicant-education';
import { ApplicantLanguage } from './applicant-language';

export class Applicant extends User {
  private _contactEmail: string;
  private _firstName: HideableField<string> = new HideableField('', false);
  private _lastName: HideableField<string> = new HideableField('', false);
  private _title: string;
  /*resume*/
  private _phone: HideableField<string> = new HideableField('', false);
  private _birthday: HideableField<number> = new HideableField(0, false);
  private _gender: HideableField<boolean> = new HideableField(true, false);
  private _city: HideableField<string> = new HideableField('', false);
  private _citizenship: HideableField<string> = new HideableField('', false);
  private _skills: ApplicantSkill[] = [];
  private _projects: ApplicantProject[] = [];
  private _professionalAreas: ProfessionalArea[] = [];
  private _educations: ApplicantEducation[] = [];
  private _languages: ApplicantLanguage[] = [];
  private _acceptableCompanyTypes: CompanyType[] = [];
  private _regionsOfInterest: Region[] = [];
  private _minSalaryExpectation: number;
  private _maxSalaryExpectation: number;
  private _availableAfterDays: number;
  private _cvUrl = '';
  private _otherInfo: string;
  private _dateExpires: number;
  private _counterViewTotal: number;
  private _counterViewSearches: number;

  constructor(id: number, email: string) {
    super();
    this.id = id;
    this.email = new HideableField<string>(email, false);
    this._contactEmail = this.email.value;
  }

  get contactEmail(): string {
    return this._contactEmail;
  }

  set contactEmail(value: string) {
    this._contactEmail = value;
  }

  get firstName(): HideableField<string> {
    return this._firstName;
  }

  set firstName(value: HideableField<string>) {
    this._firstName = value;
  }

  get lastName(): HideableField<string> {
    return this._lastName;
  }

  set lastName(value: HideableField<string>) {
    this._lastName = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get phone(): HideableField<string> {
    return this._phone;
  }

  set phone(value: HideableField<string>) {
    this._phone = value;
  }

  get birthday(): HideableField<number> {
    return this._birthday;
  }

  set birthday(value: HideableField<number>) {
    this._birthday = value;
  }

  get gender(): HideableField<boolean> {
    return this._gender;
  }

  set gender(value: HideableField<boolean>) {
    this._gender = value;
  }

  get city(): HideableField<string> {
    return this._city;
  }

  set city(value: HideableField<string>) {
    this._city = value;
  }

  get citizenship(): HideableField<string> {
    return this._citizenship;
  }

  set citizenship(value: HideableField<string>) {
    this._citizenship = value;
  }

  get skills(): ApplicantSkill[] {
    return this._skills;
  }

  set skills(value: ApplicantSkill[]) {
    this._skills = value;
  }

  get projects(): ApplicantProject[] {
    return this._projects;
  }

  set projects(value: ApplicantProject[]) {
    this._projects = value;
  }

  get educations(): ApplicantEducation[] {
    return this._educations;
  }

  set educations(value: ApplicantEducation[]) {
    this._educations = value;
  }

  get languages(): ApplicantLanguage[] {
    return this._languages;
  }

  set languages(value: ApplicantLanguage[]) {
    this._languages = value;
  }

  get professionalAreas(): ProfessionalArea[] {
    return this._professionalAreas;
  }

  set professionalAreas(value: ProfessionalArea[]) {
    this._professionalAreas = value;
  }

  get acceptableCompanyTypes(): CompanyType[] {
    return this._acceptableCompanyTypes;
  }

  set acceptableCompanyTypes(value: CompanyType[]) {
    this._acceptableCompanyTypes = value;
  }

  get regionsOfInterest(): Region[] {
    return this._regionsOfInterest;
  }

  set regionsOfInterest(value: Region[]) {
    this._regionsOfInterest = value;
  }

  get minSalaryExpectation(): number {
    return this._minSalaryExpectation;
  }

  set minSalaryExpectation(value: number) {
    this._minSalaryExpectation = value;
  }

  get maxSalaryExpectation(): number {
    return this._maxSalaryExpectation;
  }

  set maxSalaryExpectation(value: number) {
    this._maxSalaryExpectation = value;
  }

  get availableAfterDays(): number {
    return this._availableAfterDays;
  }

  set availableAfterDays(value: number) {
    this._availableAfterDays = value;
  }

  get cvUrl(): string {
    return this._cvUrl;
  }

  set cvUrl(value: string) {
    this._cvUrl = value;
  }

  get otherInfo(): string {
    return this._otherInfo;
  }

  set otherInfo(value: string) {
    this._otherInfo = value;
  }

  get dateExpires(): number {
    return this._dateExpires;
  }

  set dateExpires(value: number) {
    this._dateExpires = value;
  }

  get counterViewTotal(): number {
    return this._counterViewTotal;
  }

  set counterViewTotal(value: number) {
    this._counterViewTotal = value;
  }

  get counterViewSearches(): number {
    return this._counterViewSearches;
  }

  set counterViewSearches(value: number) {
    this._counterViewSearches = value;
  }

  private unPackField(field: any, hidden: boolean, expressions: any, fieldName: string): any {
    let result;
    if (field === undefined || field === null) {
      result = '';
    } else if (field instanceof HideableField) {
      result = this.unPackField(field.value, field.hidden, expressions, fieldName);
    } else if (field instanceof Array) {
      result = [];
      for (const elem of field) {
        result.push(this.unPackField(elem, hidden, expressions, fieldName));
      }
    } else if (typeof field === 'object') {
      result = {};
      for (const key of Object.keys(field)) {
        result[key] = this.unPackField(field[key], hidden, expressions, key);
      }
    } else {
      if (Object.keys(expressions).indexOf(fieldName) >= 0) {
        field = expressions[fieldName](field);
      }
      result = hidden ? '' : field;
    }
    return result;
  }

  /*
  getDTO transform current object to data-transfer object with hidden fields
   */
  getDTO(expressions: any): any {
    return this.unPackField(this, false, expressions, 'applicant');
  }
}
