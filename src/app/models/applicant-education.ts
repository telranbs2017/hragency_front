export class ApplicantEducation {
  constructor(
    private _id: number,
    private _description: string,
    private _university: string,
    private _dates: string
  ) {}

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get university(): string {
    return this._university;
  }

  set university(value: string) {
    this._university = value;
  }

  get dates(): string {
    return this._dates;
  }

  set dates(value: string) {
    this._dates = value;
  }

}
