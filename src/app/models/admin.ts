import { User } from './user';

export class Admin extends User {
  private _name: string;

  constructor(name: string) {
    super();
    this._name = name;
  }

  public get name(): string {
    return this._name;
  }

  public set name(value: string) {
    this._name = value;
  }

}
