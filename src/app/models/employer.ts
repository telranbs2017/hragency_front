import { CompanyType } from './enums/company-type';
import { User } from './user';
import { Region } from './enums/region';

export class Employer extends User {
  private _contactEmail: string;
  private _name: string;
  private _phone: string;
  private _companyType: CompanyType;
  private _specializations: string;
  private _region: Region;
  private _founded: number;
  private _staffNumber: number;
  private _address: string;
  private _webSite: string;

  constructor(id: number, email: string) {
    super();
    this.id = id;
    this.email.value = email;
    this._contactEmail = this.email.value;
  }

  get staffNumber(): number {
    return this._staffNumber;
  }

  set staffNumber(value: number) {
    this._staffNumber = value;
  }

  get contactEmail(): string {
    return this._contactEmail;
  }

  set contactEmail(value: string) {
    this._contactEmail = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(value: string) {
    this._phone = value;
  }

  get companyType(): CompanyType {
    return this._companyType;
  }

  set companyType(value: CompanyType) {
    this._companyType = value;
  }

  get specializations(): string {
    return this._specializations;
  }

  set specializations(value: string) {
    this._specializations = value;
  }

  get region(): Region {
    return this._region;
  }

  set region(value: Region) {
    this._region = value;
  }

  get founded(): number {
    return this._founded;
  }

  set founded(value: number) {
    this._founded = value;
  }

  get address(): string {
    return this._address;
  }

  set address(value: string) {
    this._address = value;
  }

  get webSite(): string {
    return this._webSite;
  }

  set webSite(value: string) {
    this._webSite = value;
  }
}
