import { DefaultSettings } from './enums/default-settings';

export class Field {
  fieldName: string;
}

export class ExactValues extends Field {
  values: any[];

  constructor() {
    super();
    this.values = [];
  }
}

export class NumericRange extends Field {
  min: number;
  max: number;
}

export class SkillRequest {
  // skillName: string;
  // skillId: number;
  // minExperience: number;

  constructor(public skillName: string,
              public skillId: number,
              public minExperience: number) {
    this.skillName = skillName;
    this.skillId = skillId;
    this.minExperience = minExperience;
  }
}

export class LikeFilter extends Field {
  value: string;
}

// DTO Object, used in requests to a server
export class SearchRequest {
  exactValues: ExactValues[];
  numericRanges: NumericRange[];
  skills: SkillRequest[];
  likes: LikeFilter[];
  limit: number;
  offset: number;

  constructor(params?: any) {
    if (params !== undefined) {
      this.exactValues = params.exactValues || [];
      this.numericRanges = params.numericRanges || [];
      this.skills = params.skills || [];
      this.likes = params.likes || [];
      this.limit = params.limit === undefined ? DefaultSettings.objectsPerPage : params.limit; // TODO: take this from user settings
      this.offset = params.offset || 0;
    } else {
      this.exactValues = [];
      this.numericRanges = [];
      this.skills = [];
      this.likes = [];
      this.limit = DefaultSettings.objectsPerPage;
      this.offset = 0;
    }
  }

  addExact(fieldName: string, values: any[]) {
    const v = new ExactValues();
    v.fieldName = fieldName;
    v.values = values;
    this.exactValues.push(v);
  }

  addNumericRange(fieldName: string, min: number, max: number) {
    const v = new NumericRange();
    v.fieldName = fieldName;
    v.min = min;
    v.max = max;
    this.numericRanges.push(v);
  }

  addSkill(skillName: string, skillId: number, minExperience: number) {
    const v = new SkillRequest(skillName, skillId, minExperience);
    this.skills.push(v);
  }

  addLike(fieldName: string, value: string) {
    const v = new LikeFilter();
    v.fieldName = fieldName;
    v.value = value;
    this.likes.push(v);
  }
}
