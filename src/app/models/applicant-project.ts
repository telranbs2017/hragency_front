import { HideableField } from './hideable-field';

export class ApplicantProject {
  constructor(private _id: number,
              private _title: string,
              private _description: string,
              private _company: HideableField<string> = new HideableField('', false),
              private _dates: string) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get company(): HideableField<string> {
    return this._company;
  }

  set company(value: HideableField<string>) {
    this._company = value;
  }

  get dates(): string {
    return this._dates;
  }

  set dates(value: string) {
    this._dates = value;
  }

}
