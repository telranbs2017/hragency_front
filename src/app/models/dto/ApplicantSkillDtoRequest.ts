export class ApplicantSkillDtoRequest {
    constructor(private Skill_id, private newSkill_id, private experience) {
        this.Skill_id = Skill_id;
        this.newSkill_id = newSkill_id;
        this.experience = experience;
    }
}

