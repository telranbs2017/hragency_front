export class ApplicantProjectDto {
    constructor(private id,
                private title, private description, private company, private dates, private companyIsHidden) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.company = company;
        this.dates = dates;
        this.companyIsHidden = companyIsHidden;
    }
}
