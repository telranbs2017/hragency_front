export class UserDto {
  constructor( public id, public  roleId, public  email, public  imageUrl, public  verificationStatus,
               public  verificationDate) {
    this.id = id;
    this.roleId = roleId;
    this.email = email;
    this.imageUrl = imageUrl;
    this.verificationStatus = verificationStatus;
    this.verificationDate = verificationDate;
  }
}
