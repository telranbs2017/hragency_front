
import { UserDto } from '../dto/UserDto';

export class ApplicantDto extends UserDto {
  constructor(public id, public  roleId, public  email, public  imageUrl, public verificationStatus, public  verificationDate,
              public contactEmail, public  firstName, public  lastName, public  title, public  phone, public  birthday, public  gender,
              public city, public  citizenship, public  skills, public  projects, public  professionalAreas, public  educations,
              public  languages, public acceptableCompanyTypes, public  regionsOfInterest, public  minSalaryExpectation,
              public  maxSalaryExpectation, public availableAfterDays, public  cvUrl, public  otherInfo, public  dateExpires,
              public  counterViewTotal, public  counterSearches) {
    super(id, roleId, email, imageUrl, verificationStatus, verificationDate);
    this.contactEmail = contactEmail;
    this.firstName = firstName;
    this.lastName = lastName;
    this.title = title;
    this.phone = phone;
    this.birthday = birthday;
    this.gender = gender;
    this.city = city;
    this.citizenship = citizenship;
    this.skills = skills;
    this.projects = projects;
    this.professionalAreas = professionalAreas;
    this.educations = educations;
    this.languages = languages;
    this.acceptableCompanyTypes = acceptableCompanyTypes;
    this.regionsOfInterest = regionsOfInterest;
    this.minSalaryExpectation = minSalaryExpectation;
    this.maxSalaryExpectation = maxSalaryExpectation;
    this.availableAfterDays = availableAfterDays;
    this.cvUrl = cvUrl;
    this.otherInfo = otherInfo;
    this.dateExpires = dateExpires;
    this.counterViewTotal = counterViewTotal;
    this.counterSearches = counterSearches;
  }
}
