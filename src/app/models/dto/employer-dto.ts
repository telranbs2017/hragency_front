import { Employer } from '../employer';
import { UserDto } from './UserDto';

export class EmployerDto extends UserDto {
  private contactEmail;
  private name;
  private phone;
  private CompanyType_id;
  private Region_id;
  private specializations;
  private founded;
  private staffNumber;
  private address;
  private webSite;

  constructor(employer: Employer) {
    super(employer.id,
      employer.roleId,
      employer.email.value,
      employer.imageUrl,
      employer.verificationStatus,
      employer.verificationDate);

    this.contactEmail = employer.contactEmail;
    this.name         = employer.name;
    this.phone        = employer.phone;
    this.CompanyType_id = employer.companyType;
    this.Region_id    = employer.region;
    this.specializations = employer.specializations;
    this.founded      = employer.founded;
    this.staffNumber  = employer.staffNumber;
    this.address      = employer.address;
    this.webSite      = employer.webSite;
  }
}
