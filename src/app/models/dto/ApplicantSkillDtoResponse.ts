export class ApplicantSkillDtoResponse {
    constructor(private skill, private experience, private modifiedAt) {
        this.skill = skill;
        this.experience = experience;
        this.modifiedAt = modifiedAt;
    }
}

