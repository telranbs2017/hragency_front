import { HideableField } from './hideable-field';
import { ProfileStatus } from './enums/profile-status';
import { UserRole } from './enums/user-role';

export abstract class User {
  private _id: number;
  private _email: HideableField<string> = new HideableField('', true);
  private _roleId: UserRole;
  private _imageUrl: string = '';
  private _verificationStatus: ProfileStatus;
  private _verificationDate: number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get email(): HideableField<string> {
    return this._email;
  }

  set email(value: HideableField<string>) {
    this._email = value;
  }

  get roleId(): UserRole {
    return this._roleId;
  }

  set roleId(value: UserRole) {
    this._roleId = value;
  }

  get imageUrl(): string {
    return this._imageUrl;
  }

  set imageUrl(value: string) {
    this._imageUrl = value;
  }

  get verificationStatus(): ProfileStatus {
    return this._verificationStatus;
  }

  set verificationStatus(value: ProfileStatus) {
    this._verificationStatus = value;
  }

  get verificationDate(): number {
    return this._verificationDate;
  }

  set verificationDate(value: number) {
    this._verificationDate = value;
  }

  completeAssign(...sources) {
    sources.forEach(source => {
      const descriptors = Object.keys(source).reduce((descriptor, key) => {
        descriptor[key] = Object.getOwnPropertyDescriptor(source, key);
        return descriptor;
      }, {});
      // by default, Object.assign copies enumerable Symbols too
      Object.getOwnPropertySymbols(source).forEach(sym => {
        const descriptor = Object.getOwnPropertyDescriptor(source, sym);
        if (descriptor.enumerable) {
          descriptor[sym] = descriptor;
        }
      });
      Object.defineProperties(this, descriptors);
    });
    return this;
  }
}
