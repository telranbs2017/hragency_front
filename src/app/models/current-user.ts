import { UserRole } from './enums/user-role';

export class CurrentUser {
  constructor(
    private _id: number,
    private _roleId: UserRole,
    private _email: string,
    private _imageUrl?: string
  ) {}

  public get id(): number {
    return this._id;
  }

  public set id(value: number) {
    this._id = value;
  }

  public get roleId(): UserRole {
    return this._roleId;
  }

  public set roleId(value: UserRole) {
    this._roleId = value;
  }

  public get email(): string {
    return this._email;
  }

  public set email(value: string) {
    this._email = value;
  }

  public get imageUrl(): string {
    return this._imageUrl;
  }

  public set imageUrl(value: string) {
    this._imageUrl = value;
  }
}
