export class HideableField <T> {

  constructor(private _value: T, private _hidden: boolean) {
  }

  public get value(): T {
    return this._value;
  }

  public set value(value: T) {
    this._value = value;
  }

  public get hidden(): boolean {
    return this._hidden;
  }

  public set hidden(value: boolean) {
    this._hidden = value;
  }

}
