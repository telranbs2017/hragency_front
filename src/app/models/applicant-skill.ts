import { Skill } from './skill';

export class ApplicantSkill {
  private _modifiedAt = 0;
  constructor(
    private _skill: Skill,
    private _experience?: number,
  ) { }

  get skill(): Skill {
    return this._skill;
  }

  set skill(skill: Skill) {
    this._skill = skill;
  }

  get experience(): number {
    return this._experience;
  }

  set experience(value: number) {
    this._experience = value;
  }

  get modifiedAt(): number {
    return this._modifiedAt;
  }

  set modifiedAt(value: number) {
    this._modifiedAt = value;
  }
}
