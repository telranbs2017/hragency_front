export enum CompanyType {
 'Regular IT Company' = 1,
 'Outsource company' = 2,
 'Startup company' = 3,
 'Corporate company' = 4,
 'HeadHunter Company' = 5,
 'HR Company' = 6
}
