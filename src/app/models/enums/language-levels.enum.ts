export enum LanguageLevels {
  'Beginner' = 1,
  'Intermediate' = 2,
  'Advanced' = 3,
  'Fluent' = 4,
  'Business fluent' = 5,
  'Mother Tongue' = 6
}
