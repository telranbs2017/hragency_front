export enum Region {
  'Jerusalem District' = 1,
  'Northern District' = 2,
  'Haifa District' = 3,
  'Central District' = 4,
  'Tel Aviv District' = 5,
  'Southern District' = 6,
  'Judea and Samaria Area' = 7
}
