export enum UserRole {
  Admin = 1,
  Employer = 2,
  Applicant = 3
}
