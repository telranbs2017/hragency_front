export enum ProfileStatus {
  notApproved = 0,
  approved = 2,
  partiallyApproved = 1
}
