import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import {CurrentUser} from './models/current-user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private  userService: UserService) {

   if (localStorage.getItem('user')) {
     // const obj = JSON.parse(localStorage.getItem('user'));
     // if (obj) {
     //   this.userService.user = new CurrentUser(obj.id, obj.roleId, obj.email, obj.imageUrl);
     // }
     const storedUser = JSON.parse( localStorage.getItem('user') );
     if (storedUser) {
       this.userService.user = Object.assign( new CurrentUser(0, 0, '', ''), storedUser );
     }
   }
  }
}
