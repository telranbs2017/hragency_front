import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../services/local-applicant.service';

@Component({
  selector: 'app-synchronization-indication',
  templateUrl: './synchronization-indication.component.html',
  styleUrls: ['./synchronization-indication.component.css']
})
export class SynchronizationIndicationComponent {

  constructor(public localApplicant: LocalApplicantService) {
  }

}
