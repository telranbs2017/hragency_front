import { Component, OnDestroy, OnInit } from '@angular/core';
import { ListSearchService } from '../../services/list-search.service';

@Component({
  selector: 'app-abstract-list',
  templateUrl: './abstract-list.component.html',
  styleUrls: ['./abstract-list.component.css']
})
export class AbstractListComponent implements OnInit {

  constructor(public listSearchService: ListSearchService) {
  }

  ngOnInit() {
  }

  getVal(obj: any, column): any {
    const val = obj[column];
    if (this.listSearchService.listManager.getters) {
      if (this.listSearchService.listManager.getters[column] !== undefined) {
        return this.listSearchService.listManager.getters[column](val);
      }
    }
    return val;
  }

  isClickable(obj): boolean {
    const link = this.listSearchService.listManager.getClickLink(obj);
    const r = link !== null && link !== undefined;
    return r;
  }

  // Pagination methods
  // load data to current page
  loadPage(page: number): void {
    if (page >= 0 && page < this.listSearchService.pages.length) {
      this.listSearchService.currentPage = page;
      this.listSearchService.loadObjects(this.listSearchService.currentPage * this.listSearchService.fitToPage,
        this.listSearchService.fitToPage);
    }
  }

  // disabledPage - return 'disabled' if active page is equal to param - impossible to click on element
  disabledPage(indexPage: number): string {
    return indexPage === this.listSearchService.currentPage ? 'disabled' : '';
  }

  // activePage - return 'active' if index is equal to current page
  activePage(indexPage: number): string {
    return indexPage === this.listSearchService.currentPage ? 'active' : '';
  }

}
