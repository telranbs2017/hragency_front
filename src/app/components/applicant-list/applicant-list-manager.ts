import { AbstractListManager } from '../../services/interfaces/abstract-list-manager';
import { Applicant } from '../../models/applicant';
import { ApplicantService } from '../../services/applicant.service';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';

export class ApplicantListManager extends AbstractListManager {

  constructor(private userService: UserService,
              private applicantService: ApplicantService) {
    super();
    this.hasUserApprove = true;
    this.tableColumns = [
      {key: 'firstName', title: 'First name'},
      {key: 'lastName', title: 'Last name'},
      {key: 'title', title: 'Title'}];
    this.getters = {
      firstName: this.anyName,
      lastName: this.anyName
    };
    this.actions = [];
  }

  anyName(val: any): any {
    return val.value;
  }

  getClickLink(obj: Applicant) {
    return '/' + UserRole[this.userService.user.roleId].toLowerCase() + '/applicants/' + obj.id;
  }

}
