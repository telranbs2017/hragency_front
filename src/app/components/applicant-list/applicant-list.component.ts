import { ApplicantListManager } from './applicant-list-manager';
import { ApplicantService } from '../../services/applicant.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ListSearchService } from '../../services/list-search.service';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-applicant-list',
  templateUrl: './applicant-list.component.html',
  styleUrls: ['./applicant-list.component.css']
})

export class ApplicantListComponent implements OnInit, OnDestroy {

  constructor(private listSearchService: ListSearchService,
              private userService: UserService,
              private applicantService: ApplicantService) {
  }

  ngOnInit() {
    this.listSearchService.onInitList(UserRole.Applicant,
      new ApplicantListManager(this.userService, this.applicantService));
  }

  ngOnDestroy() {
    this.listSearchService.onDestroyList(UserRole.Applicant);
  }

}
