import { SearchRequest, ExactValues, NumericRange, Field } from '../../models/search-request';

// Field in request settings
export class FieldSetting {
  // field name in request, field name in database (for example, 'name').
  fieldName: string;
  // Editor field type. Now supported 'string' and 'number'. Other will be addet later.
  type: string;
  // Label in form (for example, 'User Name').
  title: string;
  // For type select and multiselect
  // Each object mast have id and name properties
  options: Object[];
  constructor(_fieldName: string, _type: string, _title: string) {
    this.fieldName = _fieldName;
    this.type = _type;
    this.title = _title !== undefined ? _title : _fieldName;
    this.options = [];
  }
}

// Field representation in search-request-form component.
export class FieldInComponent {
  // field setting
  settings: FieldSetting;
  // current value (binded to a input in form)/
  value: any;
  // input in form id
  id: string;
  // input's type attribute in form.
  htmlType: string;
  isSingleField = false;
  isNumericRange = false;
  isSelect = false;
  isMultiple = false;
  constructor(_settings: FieldSetting, _index: number ) {
    this.settings = _settings;
    this.value = undefined;
    this.id = 'fld' + _index;
    if (this.settings.type === 'string') {
      this.htmlType = 'text';
      this.isSingleField = true;
    } else if (this.settings.type === 'number') {
      this.htmlType = 'number';
      this.isSingleField = true;
    } else if (this.settings.type === 'range') {
      this.isNumericRange = true;
      this.value = {min: undefined, max: undefined};
      this['idMin'] = this.id + 'min';
      this['idMax'] = this.id + 'max';
      this['titleMin'] = 'Min.' + this.settings.title;
      this['titleMax'] = 'Max.' + this.settings.title;
    } else if (this.settings.type === 'select') {
      this['idSelect'] = this.id + 'sel';
      this.isSelect = true;
    } else if (this.settings.type === 'multiple') {
      this['idMultiple'] = this.id + 'multiple';
      this.isMultiple = true;
      // http://materializecss.com/forms.html see select
    } else if (this.settings.type === 'like') {
      this.htmlType = 'text';
      this.isSingleField = true;
    }
  }
  // Adds item to request.
  toRequest(request: SearchRequest) {
    if (this.value !== undefined && this.value !== null && this.value !== [] && this.value !== '') {
      if (this.settings.type === 'like') {
        request.addLike(this.settings.fieldName, this.value);
      } else if (this.isSingleField || this.isSelect) {
        request.addExact(this.settings.fieldName, [this.value]);
      } else if (this.settings.type === 'range') {
        if (this.value.min !== undefined || this.value.max !== undefined) {
          request.addNumericRange(this.settings.fieldName, this.value.min, this.value.max);
        }
      } else if (this.isMultiple) {
        if (this.value !== [] && this.value !== ['']) {
          request.addExact(this.settings.fieldName, this.value);
        }
      }
    }
  }
  // Clear field's values
  clear() {
    if (this.settings.type === 'range') {
      this.value = {min: undefined, max: undefined};
    } else {
      this.value = undefined;
    }
  }
}


// Contains form settings. Can be stored in a file.
export class SearchRequestFormSettings {
  // Field settings
  fields: FieldSetting[];
  // Text on 'apply' button below the inputs.
  applyButtonText:  string;
  showSkills: boolean;
  constructor() {
    this.fields = [];
    this.applyButtonText = 'Find';
    this.showSkills = true;
  }
  add(f: FieldSetting) {
    this.fields.push(f);
  }
}
