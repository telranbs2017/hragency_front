import { Component, OnInit } from '@angular/core';
import { FieldInComponent } from './search-request-form-settings';
import { SearchRequest } from '../../models/search-request';
import { ListSearchService } from '../../services/list-search.service';

@Component({
  selector: 'app-search-request-form',
  templateUrl: './search-request-form.component.html',
  styleUrls: ['./search-request-form.component.css'],
})

export class SearchRequestFormComponent implements OnInit {

  constructor(public listSearchService: ListSearchService) {
  }

  ngOnInit() {
    // this.listSearchService.fields = [];
    // for (let i = 0; i < this.listSearchService.fields.length; i++) {
    //   const f = this.listSearchService.fields[i]; // new FieldInComponent(this.listSearchService.settings.fields[i], i);
    //   this.listSearchService.fields.push(f);
    // }
    if (this.listSearchService.fields.length === 0) {
      for (let i = 0; i < this.listSearchService.settings.fields.length; i++) {
        const f = new FieldInComponent(this.listSearchService.settings.fields[i], i);
        this.listSearchService.fields.push(f);
      }
    }
  }

  makeRequest(): SearchRequest {
    const request = new SearchRequest();
    this.listSearchService.fields.forEach(fld => fld.toRequest(request));
    if (this.listSearchService.settings.showSkills) {
      request.skills = this.listSearchService.searchService.request.skills;
    }
    request.offset = 0;
    request.limit = this.listSearchService.fitToPage;
    return request;
  }

  onSend() {
    const request = this.makeRequest();
    this.listSearchService.searchService.request = request;
    this.listSearchService.loadObjects(0, this.listSearchService.fitToPage);
    this.listSearchService.currentPage = 0;
  }

  clearField(index: number) {
    this.listSearchService.fields[index].clear();
}
}
