import { Component, OnDestroy, OnInit } from '@angular/core';
import { EmployerListManager } from './employer-list-manager';
import { EmployerService } from '../../services/employer.service';
import { ListSearchService } from '../../services/list-search.service';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-employer-list',
  templateUrl: './employer-list.component.html',
  styleUrls: ['./employer-list.component.css']
})

export class EmployerListComponent implements OnInit, OnDestroy {

  constructor(private listSearchService: ListSearchService,
              private userService: UserService,
              private employerService: EmployerService) {
  }

  ngOnInit() {
    this.listSearchService.onInitList(UserRole.Employer,
      new EmployerListManager(this.userService, this.employerService));
  }

  ngOnDestroy () {
    this.listSearchService.onDestroyList(UserRole.Employer);
  }
}
