import { AbstractListManager } from '../../services/interfaces/abstract-list-manager';
import { CompanyType } from '../../models/enums/company-type';
import { Employer } from '../../models/employer';
import { EmployerService } from '../../services/employer.service';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';

export class EmployerListManager extends AbstractListManager {

  constructor(private userService: UserService,
              private employerService: EmployerService) {
    super();
    this.hasUserApprove = true;
    this.tableColumns = [
      {key: 'name', title: 'Name'},
      {key: 'companyType', title: 'Company type'}
    ];
    this.getters = {companyType: this.getCompanyType};
    this.actions = [];
  }

  getClickLink(obj: Employer) {
    return '/' + UserRole[this.userService.user.roleId].toLowerCase() + '/employers/' + obj.id;
  }

  getCompanyType(ind: any): string {
    return CompanyType[ind];
  }

}
