import { FieldSetting, SearchRequestFormSettings } from '../search-request-form/search-request-form-settings';
import { Region } from '../../models/enums/region';
import { CompanyType } from '../../models/enums/company-type';

export class EmployerRequestObjects {
  settings = new SearchRequestFormSettings();

  constructor() {

    this.settings.add(new FieldSetting('name', 'like', 'Name'));

    const companyType = new FieldSetting('companyType', 'multiple', 'Company type');
    Object.keys(CompanyType)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => companyType.options.push({id: key, name: CompanyType[key]}));
    this.settings.add(companyType);

    this.settings.add(new FieldSetting('address', 'like', 'Address'));
    this.settings.add(new FieldSetting('webSite', 'like', 'Web Site'));
    this.settings.add(new FieldSetting('specializations', 'like', 'Specializations'));

    const region = new FieldSetting('region', 'multiple', 'Region');
    Object.keys(Region)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => region.options.push({id: key, name: Region[key]}));

    this.settings.add(region);

    this.settings.add(new FieldSetting('staffNumber', 'range', 'Stuff qty'));

    this.settings.showSkills = false;

  }
}
