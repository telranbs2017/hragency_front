import { Component, OnDestroy, OnInit } from '@angular/core';
import { EmployerRequestObjects } from './employer-request-objects';
import { EmployerSearchService } from '../../services/employer-search.service';
import { EmployerService } from '../../services/employer.service';
import { ListSearchService } from '../../services/list-search.service';
import { UserRole } from '../../models/enums/user-role';

@Component({
  selector: 'app-employer-request-form',
  templateUrl: './employer-request-form.component.html',
  styleUrls: ['./employer-request-form.component.css']
})
export class EmployerRequestFormComponent implements OnInit, OnDestroy {

  constructor(private listSearchService: ListSearchService,
              private employerService: EmployerService) {
  }

  ngOnInit() {
    this.listSearchService.onInitSearch(UserRole.Employer,
      new EmployerSearchService(this.employerService),
      new EmployerRequestObjects().settings);
  }

  ngOnDestroy () {
    this.listSearchService.onDestroySearch(UserRole.Employer);
  }
}
