import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { CvCreatorService } from '../../../../services/cv-creator.service';
import { LanguageLevels } from '../../../../models/enums/language-levels.enum';

@Component({
  selector: 'app-applicant-basic-info-view',
  templateUrl: './basic-info-view.component.html',
  styleUrls: ['./basic-info-view.component.css']
})
export class BasicInfoViewComponent implements OnInit {

  birth_date;
  languageLevels = [];

  constructor(public localApplicant: LocalApplicantService,
              private cvCreator: CvCreatorService) {
  }

  ngOnInit() {
    Object.keys(LanguageLevels)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.languageLevels.push({
        id: key,
        name: LanguageLevels[key],
      }));
  }

  birth_date_fun(value) {
    if (value === 0) {
      return '';
    }
    this.birth_date = new Date(value);
    return this.birth_date.getFullYear() + '-' +
      ('0' + (this.birth_date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.birth_date.getDate()).slice(-2);
  }

  download() {
    const expressions = {
      _birthday: (value: number) => (new Date(value)).toISOString().substring(0, 10)
    };
    this.cvCreator.generate(this.localApplicant.currentApplicant, '', expressions);
  }

}
