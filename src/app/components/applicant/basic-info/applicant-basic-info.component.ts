import { Component, OnInit } from '@angular/core';
import { Applicant } from '../../../models/applicant';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { CvCreatorService } from '../../../services/cv-creator.service';
import * as environment from '../../../../environments/environment';
import * as FileSaver from 'file-saver';
import { ApplicantService } from '../../../services/applicant.service';


@Component({
  selector: 'app-applicant-basic-info',
  templateUrl: './applicant-basic-info.component.html',
  styleUrls: ['./applicant-basic-info.component.css']
})

export class ApplicantBasicInfoComponent implements OnInit {

  applicant: Applicant;

  constructor(public localApplicant: LocalApplicantService,
              public cv: CvCreatorService,
              private applicantService: ApplicantService) {
  }

  ngOnInit() {
    this.applicant = this.localApplicant.currentApplicant;
  }

  download() {
    if (this.localApplicant.currentApplicant.cvUrl) {
      this.applicantService.downloadCv(this.localApplicant.currentApplicant.id).then(resp => {
        FileSaver.saveAs(resp,
          this.localApplicant.currentApplicant.cvUrl.slice(this.localApplicant.currentApplicant.cvUrl.lastIndexOf('/') + 1));
      }, err => {
        console.log(err);
      });

    } else {
      const expressions = {
        _birthday: (value: number) => (new Date(value)).toISOString().substring(0, 10)
      };
      this.cv.generate(this.localApplicant.currentApplicant, '', expressions);
    }
  }


}

