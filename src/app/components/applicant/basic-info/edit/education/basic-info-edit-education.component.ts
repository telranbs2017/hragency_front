import {Component, OnInit, Input} from '@angular/core';
import {LocalApplicantService} from '../../../../../services/local-applicant.service';

@Component({
  selector: 'app-basic-info-edit-education',
  templateUrl: './basic-info-edit-education.component.html',
  styleUrls: ['./basic-info-edit-education.component.css']
})
export class BasicInfoEditEducationComponent implements OnInit {

  @Input() EduInd;

  educationRecord;

  constructor(private localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
    this.educationRecord = this.localApplicant.currentApplicant.educations[this.EduInd];
  }

  deleteEducation() {
    this.localApplicant.currentApplicant.educations.splice(this.EduInd, 1);
  }


}
