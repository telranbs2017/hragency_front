import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { ApplicantLanguage } from '../../../../models/applicant-language';
import { ApplicantEducation } from '../../../../models/applicant-education';
import { ApplicantService } from '../../../../services/applicant.service';
import { environment } from '../../../../../environments/environment';


@Component({
  selector: 'app-applicant-basic-info-edit',
  templateUrl: './basic-info-edit.component.html',
  styleUrls: ['./basic-info-edit.component.css']
})


export class ApplicantBasicInfoEditComponent implements OnInit, DoCheck {
  newPhoto = false;
  birth_date;
  birth_date_format;
  fresh = true;

  constructor(public localApplicant: LocalApplicantService,
              private applicantService: ApplicantService) {

  }

  ngOnInit() {
    this.birth_date = new Date(this.localApplicant.currentApplicant.birthday.value);
    this.birth_date_format = this.birth_date.getFullYear() + '-' +
      ('0' + (this.birth_date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.birth_date.getDate()).slice(-2);
  }

  ngDoCheck() {
    if (this.localApplicant.currentApplicant.id != 0 && this.fresh) {
      this.birth_date = new Date(this.localApplicant.currentApplicant.birthday.value);
      this.birth_date_format = this.birth_date.getFullYear() + '-' +
        ('0' + (this.birth_date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.birth_date.getDate()).slice(-2);
      this.fresh = false;
    }
  }

  changeBirth(newValue) {
    this.localApplicant.currentApplicant.birthday.value = new Date(newValue).getTime();
  }

  chGender(newValue) {
    this.localApplicant.currentApplicant.gender.value = newValue;
  }

  isVis(param) {
    param.hidden = !param.hidden;
  }

  isContactEmail() {
    this.localApplicant.currentApplicant.contactEmail = this.localApplicant.currentApplicant.email.value;
  }

  addLanguage() {
    this.localApplicant.currentApplicant.languages.unshift(new ApplicantLanguage(0, '', 1));
  }

  addEducation() {
    this.localApplicant.currentApplicant.educations.unshift(new ApplicantEducation(0, '', '', ''));
  }

  addPhoto() {
    this.newPhoto = true;
  }

  saveCv(e: Event) {
    const input = e.target as HTMLInputElement;
    const file = new FormData();
    file.append('filetoupload', input.files[0], input.files[0].name);
    this.applicantService.uploadCv(file).then(x => this.localApplicant.currentApplicant.cvUrl =  x);
  }
}
