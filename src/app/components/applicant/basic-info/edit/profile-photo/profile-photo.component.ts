import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CropperSettings } from 'ng2-img-cropper';
import { ApplicantService } from '../../../../../services/applicant.service';
import { LocalApplicantService } from '../../../../../services/local-applicant.service';
import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-profile-photo',
  templateUrl: './profile-photo.component.html',
  styleUrls: ['./profile-photo.component.css']
})
export class ProfilePhotoComponent implements OnInit {
  @Output() photoUploaded = new EventEmitter();
  data: any;
  cropperSettings: CropperSettings;


  constructor(private applicantService: ApplicantService,
              private  localApplicant: LocalApplicantService) {

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 50;
    this.cropperSettings.height = 50;
    this.cropperSettings.croppedWidth = 200;
    this.cropperSettings.croppedHeight = 200;
    // this.cropperSettings.canvasWidth = 600;
    // this.cropperSettings.canvasHeight = 400;
    this.cropperSettings.rounded = true;
    this.cropperSettings.croppingClass = 'contpicture';

    this.data = {};

  }

  ngOnInit() {
  }

  savephoto() {
    const file = new FormData();
    file.append('filetoupload', this.dataURLtoFile(this.data.image, 'img.jpg'), 'img.jpg');
    this.applicantService.uploadPhoto(file).then((res) => {
      this.localApplicant.currentApplicant.imageUrl = environment.backEndHttpStatic + res;
    }, (err) => console.log(err));
    this.photoUploaded.emit();
  }

  dataURLtoFile(dataurl, filename) {
    const arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type: mime});
  }

}

