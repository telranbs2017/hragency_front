import { Component, OnInit, Input } from '@angular/core';
import { LocalApplicantService } from '../../../../../services/local-applicant.service';
import { LanguageLevels } from '../../../../../models/enums/language-levels.enum';

@Component({
  selector: 'app-basic-info-edit-languages',
  templateUrl: './basic-info-edit-languages.component.html',
  styleUrls: ['./basic-info-edit-languages.component.css']
})

export class BasicInfoEditLanguagesComponent implements OnInit {

  @Input() language;
  @Input() lIndex;

  languageLevels = [];

  constructor(public localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
    Object.keys(LanguageLevels)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.languageLevels.push({
        id: key,
        name: LanguageLevels[key],
      }));
  }

  deleteLanguage() {
    this.localApplicant.currentApplicant.languages.splice(this.lIndex, 1);
  }
}
