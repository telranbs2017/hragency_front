import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { ApplicantService } from '../../../services/applicant.service';
import { UserService } from '../../../services/user.service';
import { ApplicantProject } from '../../../models/applicant-project';
import { HideableField } from '../../../models/hideable-field';

@Component({
  selector: 'app-applicant-profile',
  templateUrl: './applicant-profile.component.html',
  styleUrls: ['./applicant-profile.component.css'],
  providers: [LocalApplicantService]
})
export class ApplicantProfileComponent implements OnInit {
  profileState = 0;
  saving = false;
  error = false;
  savingError;

  constructor(public localApplicant: LocalApplicantService,
              private applicantService: ApplicantService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.applicantService.find(this.userService.user.id)
      .then((res) => {
        this.localApplicant.currentApplicant = res;
      }, (err) => console.log(err));
    this.localApplicant.isEditable = true;
  }

  setState(i: number) {
    this.profileState = i;
  }

  addproject() {
    this.localApplicant.currentApplicant.projects.push(
      new ApplicantProject(0, '', '', new HideableField('', true), '')
    )
    ;
  }

  onDelete(i: number) {
    this.localApplicant.currentApplicant.projects.splice(i, 1);
  }

  save() {
    this.localApplicant.savingState = true;
    this.applicantService.update(this.localApplicant.currentApplicant).then((res) => {
      this.localApplicant.savingState = false;
    }, (err) => {
      this.localApplicant.savingError = err.status;
      this.localApplicant.savingState = false;
    });
  }

}
