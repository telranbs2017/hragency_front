import { Component, OnInit } from '@angular/core';
import { CvCreatorService } from '../../../services/cv-creator.service';
import { ApplicantService } from '../../../services/applicant.service';
import { LocalApplicantService } from '../../../services/local-applicant.service';

@Component({
  selector: 'app-print-or-register',
  templateUrl: './print-or-register.component.html',
  styleUrls: ['./print-or-register.component.css']
})
export class PrintOrRegisterComponent implements OnInit {
  numberOfCompanies;
  newApplicant: boolean;
  cvTamplates = [{name: 'first', address: 'input.docx'},
    {name: 'second', address: 'input1.docx'}];
  template;

  constructor(private cvCreator: CvCreatorService,
              private localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
    this.newApplicant = (this.localApplicant.currentApplicant.id === 0);
    this.numberOfCompanies = Math.floor(1000 + Math.random() * 10000); // for testing later will be call to DB
  }

  downloadCVTemplate() {
    const expressions = {
      _birthday: (value: number) => (new Date(value)).toISOString().substring(0, 10)
    };
    this.cvCreator.generate(this.localApplicant.currentApplicant, this.template || '', expressions);
  }

}
