import { Component, OnInit, Input, EventEmitter, Output, OnChanges, DoCheck, SimpleChanges } from '@angular/core';
import { ApplicantService } from '../../../../services/applicant.service';
import { ApplicantProject } from '../../../../models/applicant-project';
import { HideableField } from '../../../../models/hideable-field';
import { LocalApplicantService } from '../../../../services/local-applicant.service';

@Component({
  selector: 'app-display-applicant-project',
  templateUrl: './display-applicant-project.component.html',
  styleUrls: ['./display-applicant-project.component.css']
})
export class DisplayApplicantProjectComponent implements OnInit, DoCheck {
  @Input() project: ApplicantProject;
  @Input() i: number;
  @Output() onDelete = new EventEmitter<number>();
  emptyFields = true;
  editingstate = false;

  constructor(public localApplicant: LocalApplicantService,
              private applicantService: ApplicantService) {
  }

  ngOnInit() {
    if (this.emptyFields && this.localApplicant.isEditable) {
      this.editingstate = true;
    }
  }

  ngDoCheck() {
    this.emptyFields = this.project.title === '' ||
      this.project.dates === '' || this.project.dates === '' || this.project.description === '';
  }

  save() {
    if (!this.emptyFields) {
      if (this.editingstate) {
        if (this.localApplicant.currentApplicant.id !== 0) {
          this.localApplicant.savingState = true;
          if (this.project.id === 0) {
            const idFromServer = this.applicantService.createProject(this.project).then((res) => {
              console.log(res);
              this.localApplicant.savingState = false;
              this.project.id = res;
            }, (err) => {
              this.localApplicant.savingError = err;
            });
          } else {
            this.applicantService.updateProject(this.project).then((res) => {
              this.localApplicant.savingState = false;
            }, (err) => {
              this.localApplicant.savingError = err;
            });
          }
        }
      }
      this.editingstate = !this.editingstate;
    }
  }

  delete() {
    if (this.project.id !== 0) {
      this.applicantService.removeProject(this.project.id);
    }
    this.onDelete.emit(this.i);
  }
}
