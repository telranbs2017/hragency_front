import { Component, OnInit, Input } from '@angular/core';
import { ApplicantProject } from '../../../../models/applicant-project';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-edit-applicant-project',
  templateUrl: './edit-applicant-project.component.html',
  styleUrls: ['./edit-applicant-project.component.css']
})
export class EditApplicantProjectComponent implements OnInit {
  @Input() project: ApplicantProject;
  @Input() i;
  localProject: ApplicantProject;

  constructor(private localApplicant: LocalApplicantService, public currentUser: UserService) {
  }

  ngOnInit() {
    this.localProject = this.project;
  }

  changevisibility() {
    this.localApplicant.currentApplicant.projects[this.i].company.hidden
      = !this.localApplicant.currentApplicant.projects[this.i].company.hidden;
  }

}
