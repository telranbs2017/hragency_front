import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { ApplicantProject } from '../../../models/applicant-project';
import { HideableField } from '../../../models/hideable-field';

@Component({
  selector: 'app-applicant-project-setup',
  templateUrl: './applicant-project-setup.component.html',
  styleUrls: ['./applicant-project-setup.component.css']
})
export class ApplicantProjectSetupComponent implements OnInit {

  constructor(public localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
  }

  addproject() {
    this.localApplicant.currentApplicant.projects.push(
      new ApplicantProject(0, '', '', new HideableField('', false), '')
    )
    ;
  }

  onDelete(i: number) {
    this.localApplicant.currentApplicant.projects.splice(i, 1);
  }

}
