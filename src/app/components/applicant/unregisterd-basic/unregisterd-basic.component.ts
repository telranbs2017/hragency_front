import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { ApplicantLanguage } from '../../../models/applicant-language';
import { ApplicantEducation } from '../../../models/applicant-education';

@Component({
  selector: 'app-unregisterd-basic',
  templateUrl: './unregisterd-basic.component.html',
  styleUrls: ['./unregisterd-basic.component.css']
})
export class UnregisterdBasicComponent implements OnInit {

  birth_date;
  birth_date_format;

  constructor(public localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
    this.birth_date = new Date(this.localApplicant.currentApplicant.birthday.value);
    this.birth_date_format = this.birth_date.getFullYear() + '-' +
      ('0' + (this.birth_date.getMonth() + 1)).slice(-2) + '-' + ('0' + this.birth_date.getDate()).slice(-2);
  }

  chGender(newValue) {
    this.localApplicant.currentApplicant.gender.value = newValue;
  }

  isVis(param) {
    param.hidden = !param.hidden;
  }
  addLanguage() {
    this.localApplicant.currentApplicant.languages.unshift(new ApplicantLanguage(0, '', 1));
  }

  addEducation() {
    this.localApplicant.currentApplicant.educations.unshift(new ApplicantEducation(0, '', '', ''));
  }
  changeBirth(newValue) {
    this.localApplicant.currentApplicant.birthday.value = new Date(newValue).getTime();
  }

}
