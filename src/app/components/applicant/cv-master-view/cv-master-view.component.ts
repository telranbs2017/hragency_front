import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApplicantService } from '../../../services/applicant.service';
import { UserService } from '../../../services/user.service';
import { ApplicantProject } from '../../../models/applicant-project';
import { HideableField } from '../../../models/hideable-field';
import { Applicant } from '../../../models/applicant';
import { LocalApplicantService } from '../../../services/local-applicant.service';


@Component({
  selector: 'app-cv-master',
  templateUrl: './cv-master-view.component.html',
  styleUrls: ['./cv-master-view.component.css'],
  providers: [LocalApplicantService]
})
export class CvMasterViewComponent implements OnInit {
  filled = 0;


  constructor(private applicantService: ApplicantService,
              public localApplicant: LocalApplicantService,
              public userService: UserService) {
  }

  ngOnInit() {
    const sessionApplicant = sessionStorage.getItem('currentApplicant');
    if (sessionApplicant !== null && sessionApplicant !== undefined) {
      const tmp = this.applicantService.mapToApplicant(JSON.parse(sessionApplicant));
      this.localApplicant.currentApplicant = tmp;
    } else {
      if (this.userService.user === undefined || this.userService.user === null) {
        this.localApplicant.currentApplicant = new Applicant(0, '');

      } else {
        this.applicantService.find(this.userService.user.id)
          .then((res) => {
            this.localApplicant.currentApplicant = res;
          }, (err) => console.log(err));
      }
    }
    this.localApplicant.isEditable = true;
  }

  addproject() {
    this.localApplicant.currentApplicant.projects.push(
      new ApplicantProject(0, '', '', new HideableField('', true), '')
    )
    ;
  }

  onDelete(i: number) {
    this.localApplicant.currentApplicant.projects.splice(i, 1);
  }

  onRegister(i: number) {
    this.localApplicant.currentApplicant.id = i;
    this.localApplicant.currentApplicant.email.value = this.userService.user.email;
    this.applicantService.update(this.localApplicant.currentApplicant);
  }

  next() {
    sessionStorage.setItem('currentApplicant', JSON.stringify(this.applicantService.
    mapToApplicantDto(this.localApplicant.currentApplicant)));
    this.filled += 30;
  }

  previous() {
    sessionStorage.setItem('currentApplicant', JSON.stringify(this.applicantService.
    mapToApplicantDto(this.localApplicant.currentApplicant)));
    this.filled -= 30;
  }

  register() {
    this.filled += 10;
  }

}
