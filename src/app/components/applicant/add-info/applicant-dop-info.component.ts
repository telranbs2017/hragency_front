import { Component, OnInit } from '@angular/core';
import { Applicant } from '../../../models/applicant';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { isUndefined } from 'util';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-applicant-dop-info',
  templateUrl: './applicant-dop-info.component.html',
  styleUrls: ['./applicant-dop-info.component.css']
})
export class ApplicantDopInfoComponent implements OnInit {

  applicant: Applicant;
  view;
  newAplicant;

  constructor(public localApplicant: LocalApplicantService,
              private localuser: UserService) { }

  ngOnInit() {
    this.view = true;
    this.newAplicant = false;
    this.applicant = this.localApplicant.currentApplicant;

    if (isUndefined(this.localuser.user)) {
      this.newAplicant = !this.newAplicant;
      this.view = !this.view;
    }

  }

  edit() {
    this.view = !this.view;
  }

  save() {
    this.view = !this.view;
    this.localApplicant.currentApplicant = this.applicant;
  }

}
