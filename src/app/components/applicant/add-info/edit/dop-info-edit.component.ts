import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { Applicant } from '../../../../models/applicant';

import { ProfessionalArea } from '../../../../models/enums/professional-area';
import { CompanyType } from '../../../../models/enums/company-type';
import { Region } from '../../../../models/enums/region';

@Component({
  selector: 'app-dop-info-edit',
  templateUrl: './dop-info-edit.component.html',
  styleUrls: ['./dop-info-edit.component.css']
})
export class DopInfoEditComponent implements OnInit {

  applicant: Applicant;
  profAreas = [];
  regionsInterest = [];
  companyTypes = [];

  constructor(private localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
    this.applicant = this.localApplicant.currentApplicant;
    Object.keys(CompanyType)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.companyTypes.push({
        id: key,
        name: CompanyType[key],
        isChecked: this.checkArray(key, this.localApplicant.currentApplicant.acceptableCompanyTypes)
      }));
    Object.keys(Region)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.regionsInterest.push({
        id: key,
        name: Region[key],
        isChecked: this.checkArray(key, this.localApplicant.currentApplicant.regionsOfInterest)
      }));
    Object.keys(ProfessionalArea)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.profAreas.push({
        id: key,
        name: ProfessionalArea[key],
        isChecked: this.checkArray(key, this.localApplicant.currentApplicant.professionalAreas)
      }));
  }

  checkArray(number, array): boolean {
    let res = false;
    array.forEach(x => {
      if (x === Number(number)) {
        res = true;
      }
    });
    return res;
  }

  onSelect(element, state) {
    console.log(state);
    element.isChecked = !element.isChecked;
    if (element.isChecked) {
      state.push(Number(element.id));
    } else {
      for (let i = 0; i < state.length; i++) {
        if (state[i] === Number(element.id)) {
          state.splice(i, 1);
        }
      }
    }
    console.log(state);
  }

  makeId(n, string) {
    return string + n;
  }

}
