import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { CompanyType } from '../../../../models/enums/company-type';
import { ProfessionalArea } from '../../../../models/enums/professional-area';
import { Region } from '../../../../models/enums/region';

@Component({
  selector: 'app-dop-info-view',
  templateUrl: './dop-info-view.component.html',
  styleUrls: ['./dop-info-view.component.css']
})
export class DopInfoViewComponent implements OnInit {
  companyType = CompanyType;
  profArea = ProfessionalArea;
  regions = Region;

  constructor(public localApplicant: LocalApplicantService) {
  }

  ngOnInit() {
  }

}
