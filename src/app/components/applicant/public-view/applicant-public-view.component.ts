import { Applicant } from '../../../models/applicant';
import { ActivatedRoute } from '@angular/router';
import { ApplicantService } from '../../../services/applicant.service';
import { Component, OnInit } from '@angular/core';
import { LocalApplicantService } from '../../../services/local-applicant.service';

@Component({
  selector: 'app-applicant-public-view',
  templateUrl: './applicant-public-view.component.html',
  styleUrls: ['./applicant-public-view.component.css'],
  providers: [LocalApplicantService]
})
export class ApplicantPublicViewComponent implements OnInit {

  applicant: Applicant;

  constructor(public localApplicant: LocalApplicantService,
              private applicantService: ApplicantService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.applicant = new Applicant(0, '');
    this.applicantService.find(this.route.snapshot.params['id']).then((res: Applicant) => {
      this.localApplicant.currentApplicant = res;
      this.applicant = res;
    }, (err) => console.log(err));
    this.localApplicant.isEditable = false;
  }

}
