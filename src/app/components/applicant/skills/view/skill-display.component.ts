import { Component, OnInit, Input } from '@angular/core';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { UserRole } from '../../../../models/enums/user-role';
import { ProfileStatus } from '../../../../models/enums/profile-status';
import { UserService } from '../../../../services/user.service';
import { ApplicantService } from '../../../../services/applicant.service';

@Component({
  selector: 'app-skill-display',
  templateUrl: './skill-display.component.html',
  styleUrls: ['./skill-display.component.css']
})
export class SkillDisplayComponent implements OnInit {
  @Input() skillInd;
  @Input() applSkill;
  editState;

  constructor(public localApplicant: LocalApplicantService,
              public user: UserService,
              public applService: ApplicantService) {
  }

  ngOnInit() {
    if (this.localApplicant.currentApplicant.skills[this.skillInd].skill.name === '') {
      this.editState = true;
    }
  }

  editSkill() {
    if (this.localApplicant.currentApplicant.verificationStatus === ProfileStatus.approved) {
      if (!confirm('Editing skill will discard your skills approval. Are you sure?')) {
        return;
      } else {
        this.localApplicant.currentApplicant.verificationStatus = ProfileStatus.partiallyApproved;
      }
    }
    this.editState = !this.editState;
  }

  deleteSkill() {

    if (confirm('Delete this skill?')) {
      this.localApplicant.skillArray.push(this.applSkill.skill);
      this.localApplicant.currentApplicant.skills
        .splice(this.localApplicant.currentApplicant.skills.indexOf(this.applSkill), 1);
      if (this.localApplicant.currentApplicant.id !== 0) {
        this.localApplicant.savingState = true;
        this.applService.removeSkill(this.applSkill.skill.id).then(x => {
          this.localApplicant.savingState = false;
        }, err => this.localApplicant.savingError = err);
      }
    }
  }

  changeEditState() {
    this.editState = false;
  }
}
