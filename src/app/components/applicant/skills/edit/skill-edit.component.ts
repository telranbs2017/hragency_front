import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ApplicantSkill } from '../../../../models/applicant-skill';
import { LocalApplicantService } from '../../../../services/local-applicant.service';
import { ApplicantSkillDtoRequest } from '../../../../models/dto/ApplicantSkillDtoRequest';
import { ApplicantService } from '../../../../services/applicant.service';
import { UserService } from '../../../../services/user.service';
import { SkillService } from '../../../../services/skill.service';

@Component({
  selector: 'app-skill-edit',
  templateUrl: './skill-edit.component.html',
  styleUrls: ['./skill-edit.component.css']
})
export class SkillEditComponent implements OnInit {
  term;
  exp;
  skillId;
  skillSet = false;
  skillSetStatus = false;
  editState = true;
  skillUpd: boolean;
  oldSkillId;
  @Input() selectedSkill: ApplicantSkill;
  @Input() skillInd;
  @Output() editEmitter = new EventEmitter<boolean>();

  constructor(private localApplicant: LocalApplicantService,
              private applService: ApplicantService,
              private user: UserService,
              private renderer: Renderer2,
              private el: ElementRef,
              private skillService: SkillService) {
  }

  ngOnInit() {
    this.term = this.selectedSkill.skill.name;
    this.exp = this.selectedSkill.experience || 0;
    this.oldSkillId = this.selectedSkill.skill.id;
  }

  showSkillsList() {
    if (this.term.length >= 0) {
      this.skillSet = true;
    }
  }

  setSkill(skill) {
    this.term = skill.name;
    this.skillSetStatus = true;
    this.skillId = skill.id;
    this.skillUpd = !this.skillUpd;
    this.skillSet = false;
  }

  skillResponseAssign(applicant, skillIndex, appSkill, mod) {
    for (const aSkill of applicant.skills) {
      if (aSkill.skill.name === appSkill.skill.name) {
        aSkill.skill.id = appSkill.skill.id;
      }
    }
    applicant.skills[skillIndex] = appSkill;
    applicant.skills[skillIndex].modifiedAt = mod;
  }

  skillCreate(skillId, newSkillId, exp) {
    this.selectedSkill.skill.id = skillId;
    this.applService.createSkill(new ApplicantSkillDtoRequest(skillId,
      newSkillId, exp)).then(x => {
        this.skillResponseAssign(this.localApplicant.currentApplicant,
          this.skillInd, this.selectedSkill, x);
      },
      err => {
        this.localApplicant.savingError = err;
        for (let i = 0; i < this.localApplicant.currentApplicant.skills.length; i++) {
          if (this.localApplicant.currentApplicant.skills[i].skill.name === this.selectedSkill.skill.name) {
            this.localApplicant.currentApplicant.skills.splice(i, 1);
          }
        }
      }
    );
  }

  cancelEdit() {
    if (!this.skillUpd && this.selectedSkill.skill.id === 0) {
      this.localApplicant.currentApplicant.skills
        .splice(this.localApplicant.currentApplicant.skills.indexOf(this.selectedSkill), 1);
    }
    this.editEmitter.emit(false);
  }

  skillInputAssign(applSkill, term, exp, skillId) {
    applSkill.skill.name = term;
    applSkill.experience = exp;
    applSkill.skill.id = skillId || 0;
  }

  addNewSkill(applSkill) {
    if (applSkill.skill.id === 0) {
      this.skillService.create(applSkill.skill).then(x => {
        this.skillCreate(x, x, applSkill.experience);
      });
      return;
    }
    this.skillCreate(applSkill.skill.id,
      applSkill.skill.id, applSkill.experience);
  }

  updateCurrentSkill(applService, currentSkill, applSkill, oldId) {
    applSkill.skill.id = oldId;
    applService.updateSkill(new ApplicantSkillDtoRequest(oldId,
      applSkill.skill.id, applSkill.experience)).then(x => {
      currentSkill.modifiedAt = x;
    }, err => this.localApplicant.savingError = err);
  }

  submitSkill() {
    this.skillInputAssign(this.selectedSkill, this.term, this.exp, this.skillId);
    this.localApplicant.savingState = true;
    if (this.selectedSkill.modifiedAt === 0) {
      this.addNewSkill(this.selectedSkill);
    } else {
      if (!this.skillUpd) {
        this.updateCurrentSkill(this.applService, this.localApplicant.currentApplicant.skills[this.skillInd],
          this.selectedSkill, this.oldSkillId);
      }
    }
    this.localApplicant.savingState = false;
    if (this.user.user) {
    }
    this.localApplicant.skillArrayCleanUp(this.localApplicant.skillArray, this.selectedSkill.skill);
    this.localApplicant.currentApplicant.skills[this.skillInd] = this.selectedSkill;
    this.renderer.addClass(this.el.nativeElement, 'skillUnapproved');
    this.editEmitter.emit(false);
  }
}


