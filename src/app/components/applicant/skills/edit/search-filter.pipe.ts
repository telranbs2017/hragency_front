import { Pipe, PipeTransform, Input } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})

export class SearchFilterPipe implements PipeTransform {

  transform(skillArray: any, term: any): any {
    if (term === undefined) {
      return skillArray;
    }
    return skillArray.filter(function (skill) {
      return skill.name.toLowerCase().includes(term.toLowerCase());
    });
  }

}

