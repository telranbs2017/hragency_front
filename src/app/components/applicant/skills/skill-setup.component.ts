import { Component, OnInit } from '@angular/core';
import { Skill } from '../../../models/skill';
import { ApplicantSkill } from '../../../models/applicant-skill';
import { ProfileStatus } from '../../../models/enums/profile-status';
import { LocalApplicantService } from '../../../services/local-applicant.service';
import { UserService } from '../../../services/user.service';
import { SkillService } from '../../../services/skill.service';

@Component({
  selector: 'app-skill-setup',
  templateUrl: './skill-setup.component.html',
  styleUrls: ['./skill-setup.component.css']
})
export class SkillSetupComponent implements OnInit {
  profileStatus = ProfileStatus;

  constructor(public localApplicant: LocalApplicantService,
              public skillService: SkillService) {
  }

  ngOnInit() {
    this.localApplicant.skillArray = this.skillService.dictionary;
    for (let index = 0; index < this.localApplicant.currentApplicant.skills.length; index++) {
      this.localApplicant.skillArrayCleanUp(this.skillService.dictionary, this.localApplicant.currentApplicant.skills[index].skill);
    }
  }

  addSkill() {
    if (this.localApplicant.currentApplicant.verificationStatus === ProfileStatus.approved) {
      if (!confirm('Add a new skill? This will discard your skills approval')) {
        return;
      } else {
        this.localApplicant.currentApplicant.verificationStatus = ProfileStatus.partiallyApproved;
      }
    }
    this.localApplicant.currentApplicant.skills.unshift(new ApplicantSkill(new Skill(0, '')));
  }
}
