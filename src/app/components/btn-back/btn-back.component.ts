import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-btn-back',
  templateUrl: './btn-back.component.html',
  styleUrls: ['./btn-back.component.css']
})
export class BtnBackComponent implements OnInit {

  public btnBackIsActive = false;

  constructor() {
    if ( window.history.length ) {
      this.btnBackIsActive = true;
    }
  }

  ngOnInit() {
  }

  goBack() {
    window.history.back();
  }

}
