import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CurrentUser } from '../../models/current-user';
import { UserRole } from '../../models/enums/user-role';
import { Regex } from '../../utils/regex';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  inprogress = false;
  utypes = [];
  textError: string;
  registerForm: FormGroup;
  @Output() onRegister = new EventEmitter<number>();

  constructor(private fb: FormBuilder,
              private router: Router,
              private userService: UserService) {
    this.utypes.push({id: UserRole.Applicant, name: UserRole[UserRole.Applicant]});
    this.utypes.push({id: UserRole.Employer, name: UserRole[UserRole.Employer]});
    this.createForm();
    this.registerForm.setValue({'email': '', 'password': '', 'confPassword': '', 'roleId': this.utypes[0].id});
  }

  changeInProgress() {
    this.inprogress = !this.inprogress;
  }

  register() {
    this.inprogress = true;
    this.userService.register(this.registerForm.value).then(data => {
      if (data['id']) {
        this.userService.user = new CurrentUser(data['id'], data['roleId'], this.registerForm.value.email, data['imageUrl']);
        this.userService.startSession();
        this.onRegister.emit(data['id']);
        localStorage.setItem('user', JSON.stringify(this.userService.user));
        this.textError = '';
        this.changeInProgress();
        this.router.navigate(['/' + (UserRole[this.userService.user.roleId]).toLowerCase() + '/dashboard']);
      }
    }, err => {
      this.textError = err.error;
      console.log(err);
      this.changeInProgress();
    });
  }

  createForm() {
    this.registerForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(Regex.validEmail)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      confPassword: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      roleId: ''
    });
  }
}
