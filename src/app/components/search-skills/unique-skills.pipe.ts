import { Pipe, PipeTransform } from '@angular/core';
import { Skill } from '../../models/skill';
import { SkillRequest } from '../../models/search-request';

@Pipe({
  name: 'uniqueSkills'
})
export class UniqueSkillsPipe implements PipeTransform {

  transform(skills: Skill[], skillRequests: SkillRequest[], currentSkillName: string): Skill[] {
    if (skillRequests === undefined) {
      return skills;
    }
    currentSkillName = currentSkillName || '';

    return skills.filter(skill =>
      skill.name.toLowerCase().startsWith(currentSkillName.toLowerCase()) &&
      skillRequests.find(skillRequest => +skillRequest.skillId === +skill.id) === undefined
    );
  }

}
