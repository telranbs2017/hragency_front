import { Component, OnInit } from '@angular/core';
import { ListSearchService } from '../../services/list-search.service';
import { Skill } from '../../models/skill';
import { SkillRequest } from '../../models/search-request';
import { SkillService } from '../../services/skill.service';

@Component({
  selector: 'app-search-skills',
  templateUrl: './search-skills.component.html',
  styleUrls: ['./search-skills.component.css']
})
export class SearchSkillsComponent implements OnInit {
  currentSkillName: string;
  currentSkillId: number;

  constructor(public listSearchService: ListSearchService,
              public skillService: SkillService) {
    this.currentSkillName = '';
    this.currentSkillId = undefined;
  }

  ngOnInit() {
  }

  addSkill() {
    this.listSearchService.searchService.request.skills.push(new SkillRequest('', null, 0));
    this.currentSkillName = '';
  }

  delSkill(i: number) {
    this.listSearchService.searchService.request.skills.splice(i, 1);
    this.currentSkillName = '';
  }

  setSkill(skill: Skill, index?: number) {
    index = index || this.currentSkillId;
    this.listSearchService.searchService.request.skills[index].skillId = skill.id;
    this.listSearchService.searchService.request.skills[index].skillName = skill.name;
    this.currentSkillName = '';
  }

  skillByIndex(index: number): Skill {
    return this.skillService.dictionary.find((skillDict) =>
      skillDict.name.toLowerCase() ===
      this.listSearchService.searchService.request.skills[index].skillName.toLowerCase());
  }

  setCurrentSkillName(index: number) {
    this.currentSkillName = this.listSearchService.searchService.request.skills[index].skillName;
    this.currentSkillId = index;
  }

  onChangeSkill(index: number) {
    const skill = this.skillByIndex(index);
    if (skill === undefined) {
      this.listSearchService.searchService.request.skills[index].skillId = undefined;
    } else {
      this.setSkill(skill, index);
    }
  }
}
