import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CurrentUser } from '../../models/current-user';
import { UserRole } from '../../models/enums/user-role';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']

})

export class NavbarComponent implements OnInit {
  userRole = UserRole;
  constructor(public userService: UserService, private router: Router) {
    // const storedUser = JSON.parse( localStorage.getItem('user') );
    // if (storedUser) {
    //   this.userService.user = Object.assign( new CurrentUser(0, 0, '', ''), storedUser );
    // }
  }

  ngOnInit() {
    $('.button-collapse').sideNav({
      closeOnClick: true
    });
  }

  logOut() {
    localStorage.clear();
    // localStorage.setItem('pavremember', 'false');
    // localStorage.setItem('email', '');
    // localStorage.setItem('password', '');
    // document.cookie = 'email=""; expires=0';
    // document.cookie = 'password=""; expires=0';
    this.userService.logout();
  }
}
