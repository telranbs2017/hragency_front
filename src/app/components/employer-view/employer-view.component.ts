import { ActivatedRoute, Router } from '@angular/router';
import { CompanyType } from '../../models/enums/company-type';
import { Component, OnInit } from '@angular/core';
import { Employer } from '../../models/employer';
import { EmployerService } from '../../services/employer.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Region } from '../../models/enums/region';
import { UserService } from '../../services/user.service';
import { Regex } from '../../utils/regex';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-employer-view',
  templateUrl: './employer-view.component.html',
  styleUrls: ['./employer-view.component.css'],
  providers: [EmployerService]
})

export class EmployerViewComponent implements OnInit {
  isViewOnly = true;

  companyTypes = [];
  regions = [];

  name: FormControl;
  specializations: FormControl;
  companyType: FormControl;
  region: FormControl;
  phone: FormControl;
  founded: FormControl;
  staffNumber: FormControl;
  address: FormControl;
  contactEmail: FormControl;
  webSite: FormControl;
  employerForm: FormGroup;

  employer: Employer;

  constructor(private fb: FormBuilder,
              private employerService: EmployerService,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private toastService: ToastService) {
    this.name = new FormControl('');
    this.specializations = new FormControl('');
    this.companyType = new FormControl('');
    this.region = new FormControl('');
    this.phone = new FormControl('');
    this.founded = new FormControl('');
    this.staffNumber = new FormControl('');
    this.address = new FormControl('');
    this.contactEmail = new FormControl('',
      Validators.compose([Validators.required, Validators.pattern(Regex.validEmail)]));
    this.webSite = new FormControl('');
    this.createForm();
    this.employerForm.setValue({
      'name': '',
      'specializations': '',
      'companyType': '',
      'region': '',
      'phone': '',
      'founded': '',
      'staffNumber': '',
      'address': '',
      'contactEmail': '',
      'webSite': ''
    });
  }

  ngOnInit() {
    let id: number;
    if (this.route.snapshot.paramMap.get('id') === null) {
      id = this.userService.user.id;
      this.isViewOnly = false;
    } else {
      id = +this.route.snapshot.paramMap.get('id');
      this.isViewOnly = true;
    }

    this.employer = new Employer(0, '');

    this.employerService.find(id)
      .then((employer: Employer) => {
        this.employer = employer;
        this.employerForm.setValue({
          'name': this.employer.name,
          'specializations': this.employer.specializations,
          'companyType': this.employer.companyType,
          'region': this.employer.region,
          'phone': this.employer.phone,
          'founded': this.employer.founded,
          'staffNumber': this.employer.staffNumber,
          'address': this.employer.address,
          'contactEmail': this.employer.contactEmail,
          'webSite': this.employer.webSite
        });
      }, error => console.log(error));
    this.setEnable();

    Object.keys(CompanyType)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.companyTypes.push({key: key, name: CompanyType[key]}));

    Object.keys(Region)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => this.regions.push({key: key, name: Region[key]}));

  }

  setEnable() {
    if (this.isViewOnly) {
      this.employerForm.disable();
    } else {
      this.employerForm.enable();
    }
  }

  submit() {
    this.employer.name = this.name.value;
    this.employer.specializations = this.specializations.value;
    this.employer.companyType = this.companyType.value;
    this.employer.region = this.region.value;
    this.employer.phone = this.phone.value;
    this.employer.founded = this.founded.value;
    this.employer.staffNumber = this.staffNumber.value;
    this.employer.address = this.address.value;
    this.employer.contactEmail = this.contactEmail.value;
    this.employer.webSite = this.webSite.value;
    this.employerService.update(this.employer)
      .then(response => {
        this.toastService.toast('Changes saved');
      })
      .catch(error => {
        console.log(error);
        this.toastService.toast('Data didn\'t save. Please try later');
      });
  }

  createForm() {
    this.employerForm = this.fb.group({
      name: this.name,
      specializations: this.specializations,
      companyType: this.companyType,
      region: this.region,
      phone: this.phone,
      founded: this.founded,
      staffNumber: this.staffNumber,
      address: this.address,
      contactEmail: this.contactEmail,
      webSite: this.webSite
    });
  }
}
