import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { CurrentUser } from '../../models/current-user';
import { UserRole } from '../../models/enums/user-role';
import { Regex } from '../../utils/regex';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  inprogress = false;
  textError: string;
  loginForm: FormGroup;

  constructor(private userService: UserService, private  router: Router) {
    this.loginForm = new FormGroup({

      'email': new FormControl('', [
        Validators.required,
        Validators.pattern(Regex.validEmail)
      ]),

      'password': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]),
      'remember': new FormControl('')
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('pavremember') === 'true') {
      this.loginForm.setValue({'email' : localStorage.getItem('email') ,
        'password' : localStorage.getItem('password'),
        'remember' : localStorage.getItem('pavremember')});
      this.login();
    }
  }

  remChange() {
    localStorage.setItem('pavremember', this.loginForm.value.remember);
    if (Boolean(this.loginForm.value.remember)) {
      localStorage.setItem('email', this.loginForm.value.email);
      localStorage.setItem('password', this.loginForm.value.password);
    } else {
      localStorage.setItem('email', '');
      localStorage.setItem('password', '');
    }
  }

  changeInProgress() {
    this.inprogress = !this.inprogress;
  }

  login() {
    this.inprogress = true;
    this.remChange();
    this.userService.authenticate(this.loginForm.value).then((data: any) => {
      if (data['id']) {
        this.userService.user = new CurrentUser(data['id'], data['roleId'], this.loginForm.value.email, data['imageUrl']);
        localStorage.setItem('user', JSON.stringify(this.userService.user));
        // this.router.navigate(['/' + (UserRole[this.userService.user.roleId]).toLowerCase() + '/dashboard']);
        const redirect = this.userService.redirectUrl
          ? this.userService.redirectUrl : '/' + (UserRole[this.userService.user.roleId]).toLowerCase() + '/dashboard';
        this.router.navigate([redirect]);
        this.textError = '';
        this.changeInProgress();
      }
    }, err => {
      this.textError = err.error;
      console.log(err);
      this.changeInProgress();
    });
  }
}

