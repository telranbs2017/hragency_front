import { Component, Input, OnInit } from '@angular/core';
import { Applicant } from '../../models/applicant';
import { Employer } from '../../models/employer';
import { ProfileStatus } from '../../models/enums/profile-status';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-approve',
  templateUrl: './user-approve.component.html',
  styleUrls: ['./user-approve.component.css']
})

export class UserApproveComponent implements OnInit {
  @Input() user: Applicant | Employer;

  public profileStatus = ProfileStatus;

  inProcess = false;

  status = [{icon: 'error', title: 'not approved', color: 'red'},
    {icon: 'help', title: 'partially approved', color: 'amber'},
    {icon: 'done', title: 'approved', color: 'green'}];

  constructor(private userService: UserService) {
    this.user = new Applicant(0, '');
    this.user.verificationStatus = this.user.verificationStatus || ProfileStatus.notApproved;
  }

  ngOnInit() {
  }

  changeStatus(newVerificationStatus: number) {
    if (this.userService.user.roleId !== UserRole.Admin) {
      return;
    }
    this.inProcess = true;
    // let newVerificationStatus = this.user.verificationStatus + 1;
    // if (newVerificationStatus >= this.status.length) {
    //   newVerificationStatus = 0;
    // }

    this.userService.approve({id: this.user.id, verificationStatus: newVerificationStatus})
      .then(verificationDate => {
        this.user.verificationStatus = newVerificationStatus;
        this.user.verificationDate = verificationDate;
        this.inProcess = false;
      })
      .catch(error => {
        console.log(error);
        this.inProcess = false;
      });
  }

  isLoading(): string {
    return this.inProcess ? 'pulse' : '';
  }
}
