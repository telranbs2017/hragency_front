import { FieldSetting, SearchRequestFormSettings } from '../search-request-form/search-request-form-settings';
import { Region } from '../../models/enums/region';
import { ProfessionalArea } from '../../models/enums/professional-area';

export class ApplicantRequestObjects {
  settings = new SearchRequestFormSettings();

  constructor() {

    this.settings.add(new FieldSetting('title', 'like', 'Title'));

    const profArea = new FieldSetting('ProfessionalArea.id', 'multiple', 'Prof.area');
    Object.keys(ProfessionalArea)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => profArea.options.push({id: key, name: ProfessionalArea[key]}));
    this.settings.add(profArea);

    const region = new FieldSetting('Region.id', 'multiple', 'Region');
    Object.keys(Region)
      .filter(key => !isNaN(Number(key)))
      .forEach(key => region.options.push({id: key, name: Region[key]}));
    this.settings.add(region);

    this.settings.add(new FieldSetting('minSalaryExpectation', 'range', '$'));

    // this.settings.add(new FieldSetting('firstName', 'like', 'First Name'));
    // this.settings.add(new FieldSetting('lastName', 'like', 'Last Name'));
    this.settings.add(new FieldSetting('city', 'like', 'City'));
    this.settings.add(new FieldSetting('citizenship', 'like', 'Citizenship'));
    // this.settings.add(new FieldSetting('ApplicantEducation.university', 'like', 'Education. University'));
    // this.settings.add(new FieldSetting('ApplicantEducation.description', 'like', 'Education. Description'));
    // this.settings.add(new FieldSetting('ApplicantProject.title', 'like', 'Job/Projects. Position'));
    // this.settings.add(new FieldSetting('ApplicantProject.company', 'like', 'Job/Projects. Company name'));
    // this.settings.add(new FieldSetting('ApplicantProject.description', 'like', 'Job/Projects. Description'));
    // this.settings.add(new FieldSetting('ApplicantLanguage.language', 'like', 'Language'));

    this.settings.showSkills = true;

  }
}

