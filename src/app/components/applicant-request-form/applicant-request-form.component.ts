import { ApplicantRequestObjects } from './applicant-request-objects';
import { ApplicantSearchService } from '../../services/applicant-search.service';
import { ApplicantService } from '../../services/applicant.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ListSearchService } from '../../services/list-search.service';
import { UserRole } from '../../models/enums/user-role';

@Component({
  selector: 'app-applicant-request-form',
  templateUrl: './applicant-request-form.component.html',
  styleUrls: ['./applicant-request-form.component.css']
})
export class ApplicantRequestFormComponent implements OnInit, OnDestroy {

  constructor(private listSearchService: ListSearchService,
              private applicantService: ApplicantService) {
  }

  ngOnInit() {
    this.listSearchService.onInitSearch(UserRole.Applicant,
      new ApplicantSearchService(this.applicantService),
      new ApplicantRequestObjects().settings);
  }

  ngOnDestroy () {
    this.listSearchService.onDestroySearch(UserRole.Applicant);
  }
}
