import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-menu',
  templateUrl: './admin-side-menu.component.html',
  styleUrls: ['./admin-side-menu.component.css']
})
export class AdminSideMenuComponent implements OnInit {
  menuItems = [
    {routerLink: ['/admin/applicants'], materialIcons: 'search', text: 'Find applicants'},
    {routerLink: ['/admin/employers'], materialIcons: 'search', text: 'Find employers'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
