import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-applicant-side-menu',
  templateUrl: './applicant-side-menu.component.html',
  styleUrls: ['./applicant-side-menu.component.css']
})
export class ApplicantSideMenuComponent implements OnInit {
  menuItems = [
    {routerLink: ['/applicant/profile'], materialIcons: 'edit', text: 'Edit profile'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
