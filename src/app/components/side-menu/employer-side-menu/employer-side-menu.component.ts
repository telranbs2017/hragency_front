import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employer-side-menu',
  templateUrl: './employer-side-menu.component.html',
  styleUrls: ['./employer-side-menu.component.css']
})
export class EmployerSideMenuComponent implements OnInit {
  menuItems = [
    {routerLink: ['/employer/profile'], materialIcons: 'account_circle', text: 'Edit profile'},
    {routerLink: ['/employer/applicants'], materialIcons: 'search', text: 'Find applicants'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
