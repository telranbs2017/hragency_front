import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { UserService } from '../../../services/user.service';
import { DashboardItem } from '../dashboard-item';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.css']
})
export class DashboardViewComponent implements OnInit {

  constructor(private userService: UserService, private dashboardService: DashboardService) {
    this.dashboardItems = this.dashboardService.dashboardItems;
  }

  dashboardItems: DashboardItem[];

  ngOnInit() {
  }
}
