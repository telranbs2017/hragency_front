import { DashboardItem } from './dashboard-item';

export const DASHBOARDITEMS: DashboardItem[] = [
  // {id: '0', legend: 'Registered users:', iconName: 'people', response: '100500'},
  // {id: '1', legend: 'Visitors', iconName: 'visibility', response: '21'},
  // {id: '2', legend: 'Users online', iconName: 'people_outline', response: '500'},
  // {id: '3', legend: 'Days to profile deactivation :', iconName: 'update', response: '7'},
  // {id: '4', legend: 'Top 5 searches', iconName: 'search', response: 'Java, C++, SQL, C#, BASIC'},
  // {id: '5', legend: 'Advanced statistics', iconName: 'info_outline', response: 'Tap to see'},
];
