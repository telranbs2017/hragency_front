import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { UserRole } from '../../models/enums/user-role';
import { UserService } from '../../services/user.service';
import { Map } from 'rxjs/util/Map';

@Injectable()
export class DashboardService {
  public currentItem = {
    id: '',
    legend: '',
    iconName: '',
    response: '',
    showLink: true,
    details: ''
  };
  public dashboardItemsDetails: Map<string, DashboardItem>;
  dashboardItems = [
      {
        id: 'totalApplicants',
        legend: 'Total Applicants',
        iconName: 'people',
        response: '',
        showLink: false,
        details: ''
      }
      ,
      {
        id: 'totalEmployers',
        legend: 'Total Employers',
        iconName: 'people',
        response: '',
        showLink: false,
        details: ''
      },
      {
        id: 'viewCount',
        legend: 'Visitors',
        iconName: 'visibility',
        response: '',
        showLink: true,
        details: ''
      },
      {
        id: 'onlineUsers',
        legend: 'Users online',
        iconName: 'people_outline',
        response: '',
        showLink: false,
        details: ''
      },
      {
        id: 'daysUntilDeactivation',
        legend: 'Days to profile deactivation :',
        iconName: 'update',
        response: '',
        showLink: false,
        details: ''
      },
      {
        id: '',
        legend: 'Top 5 searches',
        iconName: 'search',
        response: '',
        showLink: true,
        details: ''
      },
      {
        id: 'upcomingEvents',
        legend: 'Upcoming Events List',
        iconName: 'info_outline',
        response: '',
        showLink: true,
        details: ''
      },
      {
        id: 'newApplBySkills',
        legend: 'New Applicants List',
        iconName: 'info_outline',
        response: '',
        showLink: true,
        details: ''
      },
      {
        id: 'applExpectedSalary',
        legend: 'Applicants Expected Salary',
        iconName: 'info_outline',
        response: '',
        showLink: false,
        details: ''
      },
      {
        id: 'nonApprovedSkillsReq',
        legend: 'New Skills to Approve',
        iconName: 'info_outline',
        response: '',
        showLink: true,
        details: ''
      },
      {
        id: 'verifyQueueCount',
        legend: 'New Persons to Verify',
        iconName: 'info_outline',
        response: '',
        showLink: false,
        details: ''
      }
    ];

  constructor(private http: HttpClient, private userService: UserService) {
    // console.log('dashboard service');
   this.dashboardItemsDetails = new Map();
    this.putDashboardItemsDetails();
  }

  putDashboardItemsDetails() {
          this.getDashboardCards();
  }


  getUserRole(): string {
    return (UserRole[this.userService.user.roleId]).toLowerCase();
  }

  getDashboardCards(): Promise<any> {
    return this.http.get(
      environment.backEndHttpsUrl + 'statistics/' + this.getUserRole(),
      {withCredentials: true}
    ).toPromise().then(data => {
      // console.log(data);
      for (const prop in data) {
        for (let i = 0; i < this.dashboardItems.length; i++) {
          if (this.dashboardItems[i].id === prop) {
            // console.log(this.dashboardItems[i]);
            if (!Array.isArray(data[prop])) {
              this.dashboardItems[i].response = data[prop];
            } else {
              // console.log(this.dashboardItems[i].id, this.dashboardItems[i])
              this.dashboardItems[i].response = data[prop].length;
              this.dashboardItems[i].details = data[prop];
              this.dashboardItemsDetails.set(this.dashboardItems[i].id, this.dashboardItems[i]);
            }
          }
        }
      }
      // console.log(this.dashboardItemsDetails);
    }, err => {
      // console.log(err);
    }).then(
      x => Promise.resolve(true)
    );
  }

  getDashboardItemById(id: string) {
    this.currentItem = this.dashboardItemsDetails.get(id);
    return this.dashboardItemsDetails.get(id);
  }

  setCurrentItem(id: string) {
    // console.log('set current item to ' + id);
    // console.log(this.currentItem);
    this.currentItem = this.dashboardItemsDetails.get(id);
    // console.log(this.currentItem);
  }

}

class DashboardItem {
  id: string;
  legend: string;
  iconName: string;
  response: string;
  showLink: boolean;
  details: any;
}
