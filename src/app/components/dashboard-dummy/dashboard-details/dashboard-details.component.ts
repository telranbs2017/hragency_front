import { Component, OnInit, Input } from '@angular/core';
import { DashboardItem } from '../dashboard-item';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { DashboardViewComponent } from '../dashboard-view/dashboard-view.component';

@Component({
  selector: 'app-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.css']
})

export class DashboardDetailsComponent implements OnInit {
  @Input() dashboardItems: DashboardItem;

  dashboardItem: DashboardItem;

  constructor(private route: ActivatedRoute,
              public dashboardService: DashboardService,
              ) {
    if (!dashboardService.dashboardItemsDetails.size) {
      dashboardService.getDashboardCards()
        .then( x => this.updateCurrentItem());
    } else {
      this.updateCurrentItem();
    }
  }


  ngOnInit(): void {
  }

  updateCurrentItem(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.dashboardService.setCurrentItem(id);
  }
}
