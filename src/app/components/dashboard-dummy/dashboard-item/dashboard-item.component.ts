import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-dashboard-item',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.css']
})
export class DashboardItemComponent implements OnInit {

  @Input() params = {request: () => Promise.resolve(''), iconName: '', legend: '', id: 0, response: '', showLink: false};
  res = '';

  userRoleId: string;

  constructor(private dashboardService: DashboardService) {
    this.userRoleId = this.dashboardService.getUserRole();
  }

  ngOnInit() {
    // console.log(this.dashboardService[this.params.]());
    // this.params.request().then(result => this.res = result, err => console.log(err));
  }

}
