export class DashboardItem {
  id: string;
  legend: string;
  iconName: string;
  response: string;
  showLink: boolean;
  details: any;
}
