export class Random {
  static nextInt(min, max): number {
    if (min > max) {
      const packet = min;
      min = max;
      max = packet;
    }
    return min + Math.floor(Math.random() * (max - min));
  }

  static nextDouble(min, max): number {
    return min + Math.random() * (max - min);
  }

  static nextBoolean(): boolean {
    return Math.random() >= 0.5;
  }

  static nextDate(min, max): Date {
    const between = (max - min) / 1000 / 60 / 60 / 24;
    const res = new Date(min);
    res.setDate(res.getDate() + Math.floor(Math.random() * between));
    return res;
  }

  static nextString(lMin, lMax): string {
    const len = lMin + Math.floor(Math.random() * (lMax - lMin + 1));
    let res = '';

    while (res.length < len) {
      res = res + Math.random().toString(36).substring(2);
    }
    return res.substring(0, len);
  }

  static nextText(nMin, nMax, lMin, lMax): string {
    const num = nMin + Math.floor(Math.random() * (nMax - nMin + 1));
    const res = [];
    for (let i = 0; i < num; i++) {
      res.push(this.nextString(lMin, lMax));
    }
    return res.join(' ');
  }
}
