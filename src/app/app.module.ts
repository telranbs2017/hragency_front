import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ImageCropperModule } from 'ng2-img-cropper';
import { MaterializeModule } from 'angular2-materialize';
import { NgModule } from '@angular/core';

import { AbstractListComponent } from './components/abstract-list/abstract-list.component';
import { AdminSideMenuComponent } from './components/side-menu/admin-side-menu/admin-side-menu.component';
import { AppComponent } from './app.component';
import { ApplicantDopInfoComponent } from './components/applicant/add-info/applicant-dop-info.component';
import { ApplicantBasicInfoComponent } from './components/applicant/basic-info/applicant-basic-info.component';
import { ApplicantBasicInfoEditComponent } from './components/applicant/basic-info/edit/basic-info-edit.component';
import { ApplicantListComponent } from './components/applicant-list/applicant-list.component';
import { ApplicantProfileComponent } from './components/applicant/profile/applicant-profile.component';
import { ApplicantProjectSetupComponent } from './components/applicant/projects/applicant-project-setup.component';
import { ApplicantPublicViewComponent } from './components/applicant/public-view/applicant-public-view.component';
import { ApplicantRequestFormComponent } from './components/applicant-request-form/applicant-request-form.component';
import { ApplicantSideMenuComponent } from './components/side-menu/applicant-side-menu/applicant-side-menu.component';
import { BasicInfoViewComponent } from './components/applicant/basic-info/view/basic-info-view.component';
import { BasicInfoEditLanguagesComponent } from './components/applicant/basic-info/edit/languages/basic-info-edit-languages.component';
import { BasicInfoEditEducationComponent } from './components/applicant/basic-info/edit/education/basic-info-edit-education.component';
import { BtnBackComponent } from './components/btn-back/btn-back.component';
import { CvMasterViewComponent } from './components/applicant/cv-master-view/cv-master-view.component';
import { DashboardDetailsComponent } from './components/dashboard-dummy/dashboard-details/dashboard-details.component';
import { DashboardViewComponent } from './components/dashboard-dummy/dashboard-view/dashboard-view.component';
import { DashboardItemComponent } from './components/dashboard-dummy/dashboard-item/dashboard-item.component';
import { DisplayApplicantProjectComponent } from './components/applicant/projects/view/display-applicant-project.component';
import { DopInfoEditComponent } from './components/applicant/add-info/edit/dop-info-edit.component';
import { DopInfoViewComponent } from './components/applicant/add-info/view/dop-info-view.component';
import { EditApplicantProjectComponent } from './components/applicant/projects/edit/edit-applicant-project.component';
import { EmployerListComponent } from './components/employer-list/employer-list.component';
import { EmployerRequestFormComponent } from './components/employer-request-form/employer-request-form.component';
import { EmployerSideMenuComponent } from './components/side-menu/employer-side-menu/employer-side-menu.component';
import { EmployerViewComponent } from './components/employer-view/employer-view.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PrintOrRegisterComponent } from './components/applicant/print-or-register/print-or-register.component';
import { ProfilePhotoComponent } from './components/applicant/basic-info/edit/profile-photo/profile-photo.component';
import { RegisterComponent } from './components/register/register.component';
import { SearchRequestFormComponent } from './components/search-request-form/search-request-form.component';
import { SearchSkillsComponent } from './components/search-skills/search-skills.component';
import { SkillDisplayComponent } from './components/applicant/skills/view/skill-display.component';
import { SkillEditComponent } from './components/applicant/skills/edit/skill-edit.component';
import { SkillSetupComponent } from './components/applicant/skills/skill-setup.component';
import { SynchronizationIndicationComponent } from './components/synchronization-indication/synchronization-indication.component';
import { UserApproveComponent } from './components/user-approve/user-approve.component';

import { ApplicantService } from './services/applicant.service';
import { AuthGuardService } from './services/auth-guard.service';
import { CvCreatorService } from './services/cv-creator.service';
import { DashboardService } from './components/dashboard-dummy/dashboard.service';
import { EmployerService } from './services/employer.service';
import { ListSearchService } from './services/list-search.service';
import { SkillService } from './services/skill.service';
import {ToastService} from './services/toast.service';
import { UserService } from './services/user.service';

import { OrderByPipe } from './components/applicant/skills/edit/order-by.pipe';
import { SearchFilterPipe } from './components/applicant/skills/edit/search-filter.pipe';
import { UniqueSkillsPipe } from './components/search-skills/unique-skills.pipe';
import { UnregisterdBasicComponent } from './components/applicant/unregisterd-basic/unregisterd-basic.component';

@NgModule({
  declarations: [
    AbstractListComponent,
    AdminSideMenuComponent,
    AppComponent,
    ApplicantBasicInfoComponent,
    ApplicantBasicInfoEditComponent,
    ApplicantDopInfoComponent,
    ApplicantListComponent,
    ApplicantProfileComponent,
    ApplicantProjectSetupComponent,
    ApplicantPublicViewComponent,
    ApplicantRequestFormComponent,
    ApplicantSideMenuComponent,
    BasicInfoEditLanguagesComponent,
    BasicInfoEditEducationComponent,
    BasicInfoEditEducationComponent,
    BasicInfoViewComponent,
    BtnBackComponent,
    CvMasterViewComponent,
    DashboardDetailsComponent,
    DashboardViewComponent,
    DashboardItemComponent,
    DisplayApplicantProjectComponent,
    DopInfoEditComponent,
    DopInfoViewComponent,
    EditApplicantProjectComponent,
    EmployerListComponent,
    EmployerRequestFormComponent,
    EmployerSideMenuComponent,
    EmployerViewComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    OrderByPipe,
    PrintOrRegisterComponent,
    ProfilePhotoComponent,
    RegisterComponent,
    SearchFilterPipe,
    SearchRequestFormComponent,
    SearchSkillsComponent,
    SkillDisplayComponent,
    SkillEditComponent,
    SynchronizationIndicationComponent,
    SkillSetupComponent,
    UniqueSkillsPipe,
    UserApproveComponent,
    UnregisterdBasicComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ImageCropperModule,
    MaterializeModule,
    ReactiveFormsModule
  ],
  providers: [
    ApplicantService,
    AuthGuardService,
    CvCreatorService,
    DashboardService,
    EmployerService,
    ListSearchService,
    SkillService,
    ToastService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
