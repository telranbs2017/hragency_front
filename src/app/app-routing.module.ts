import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminSideMenuComponent } from './components/side-menu/admin-side-menu/admin-side-menu.component';
import { ApplicantListComponent } from './components/applicant-list/applicant-list.component';
import { ApplicantPublicViewComponent } from './components/applicant/public-view/applicant-public-view.component';
import { ApplicantRequestFormComponent } from './components/applicant-request-form/applicant-request-form.component';
import { ApplicantSideMenuComponent } from './components/side-menu/applicant-side-menu/applicant-side-menu.component';
import { CvMasterViewComponent } from './components/applicant/cv-master-view/cv-master-view.component';
import { DashboardViewComponent } from './components/dashboard-dummy/dashboard-view/dashboard-view.component';
import { EmployerListComponent } from './components/employer-list/employer-list.component';
import { EmployerRequestFormComponent } from './components/employer-request-form/employer-request-form.component';
import { EmployerSideMenuComponent } from './components/side-menu/employer-side-menu/employer-side-menu.component';
import { EmployerViewComponent } from './components/employer-view/employer-view.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ApplicantProfileComponent } from './components/applicant/profile/applicant-profile.component';
import {AuthGuardService} from './services/auth-guard.service';
import { DashboardDetailsComponent } from './components/dashboard-dummy/dashboard-details/dashboard-details.component';

const appRoutes: Routes = [
  { path: 'applicant', canActivate: [AuthGuardService], children: [
    { path: 'dashboard', component: DashboardViewComponent },
    { path: 'dashboard/:id', component: DashboardDetailsComponent },
    { path: 'profile', component: ApplicantProfileComponent },
    { path: '', component: ApplicantSideMenuComponent, outlet: '~' }
  ] },
  { path: 'employer/dashboard', canActivate: [AuthGuardService], children: [
    { path: '', component: DashboardViewComponent },
    { path: '', component: EmployerSideMenuComponent, outlet: '~' }
  ] },
  { path: 'employer/dashboard/:id', canActivate: [AuthGuardService], children: [
    { path: '', component: DashboardDetailsComponent },
    { path: '', component: EmployerSideMenuComponent, outlet: '~' }
  ] },
  { path: 'employer/profile', canActivate: [AuthGuardService], children: [
    { path: '', component: EmployerViewComponent },
    { path: '', component: EmployerSideMenuComponent, outlet: '~' }
  ] },
  { path: 'employer/applicants/:id', canActivate: [AuthGuardService], children: [
    { path: '', component: ApplicantPublicViewComponent }
    // ,{ path: '', component: EmployerSideMenuComponent, outlet: '~' }
  ] },
  { path: 'employer/applicants', canActivate: [AuthGuardService], children: [
    { path: '', component: ApplicantListComponent },
    { path: '', component: ApplicantRequestFormComponent, outlet: '~' }
  ] },
  { path: 'admin/dashboard', canActivate: [AuthGuardService], children: [
    { path: '', component: DashboardViewComponent },
    { path: '', component: AdminSideMenuComponent, outlet: '~' }
  ] },
  { path: 'admin/dashboard/:id', canActivate: [AuthGuardService], children: [
      { path: '', component: DashboardDetailsComponent },
      { path: '', component: EmployerSideMenuComponent, outlet: '~' }
    ] },
  { path: 'admin/applicants/:id', canActivate: [AuthGuardService], children: [
    { path: '', component: ApplicantPublicViewComponent }
    // , { path: '', component: AdminSideMenuComponent, outlet: '~' }
  ] },
  { path: 'admin/applicants', canActivate: [AuthGuardService], children: [
    { path: '', component: ApplicantListComponent },
    { path: '', component: ApplicantRequestFormComponent, outlet: '~' }
  ] },
  { path: 'admin/employers/:id', canActivate: [AuthGuardService], children: [
    { path: '', component: EmployerViewComponent },
    { path: '', component: AdminSideMenuComponent, outlet: '~' }
  ] },
  { path: 'admin/employers', canActivate: [AuthGuardService], children: [
    { path: '', component: EmployerListComponent },
    { path: '', component: EmployerRequestFormComponent, outlet: '~' }
  ] },
  { path: 'cv-master', component: CvMasterViewComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
